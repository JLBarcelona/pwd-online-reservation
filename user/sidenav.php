 <style type="text/css">
    .img_profile{
     width:50px;
     height: 40px;
    }
 </style>
  <!-- Sidebar -->
    <ul class="navbar-nav bg-info sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-icon ">
         <img class="img-fluid rounded-circle my_avatar img_profile" width="30" src="../webroot/img/site/<?php echo $auth['gender'].'.png' ?>">
        </div>
        <div class="sidebar-brand-text mx-3 text-truncate"><?php echo 'Hi, '.$_SESSION['firstname'].''; ?></div>  
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">
      <!-- Nav Item - Dashboard -->
      <li class="nav-item ">
        <a class="nav-link" href="index.php">
          <i class="fas fa-fw fa-list"></i>
          <span>My Request</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        File Manager
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link" href="files.php"  aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-folder"></i>
          <span>My Requirements</span>
        </a>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->

      <!-- Divider -->
      <hr class="sidebar-divider">

       <div class="sidebar-heading">
        Account Settings
      </div>

       <li class="nav-item">
        <a class="nav-link" href="setting.php"  aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cogs "></i>
          <span>Settings</span>
        </a>
      </li>


       <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-backdrop="static" data-target="#logoutModal"  aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-sign-out-alt "></i>
          <span>Logout</span>
        </a>
      </li>


         <!-- Nav Item - Pages Collapse Menu -->
      
      <br>

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Messages -->
          <!--   <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i> -->
                <!-- Counter - Messages -->
             <!--    <span class="badge badge-danger badge-counter">0</span>
              </a> -->
              <!-- Dropdown - Messages -->
             <!--  <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                <h6 class="dropdown-header">
                  Notification
                </h6> -->
                <!-- alert -->
              <!--   <a class="dropdown-item d-flex align-items-center" href="#">
                  <div class="dropdown-list-image mr-3">
                     <div class="icon-circle bg-primary">
                      <i class="fas fa-bell text-white"></i>
                    </div>
                  </div>

                  <div class="font-weight-bold">
                    <div class="text-truncate">1 New Request.</div>
                    <div class="small text-gray-500">Jeffrey Luis · 58m</div>
                  </div>
                </a> -->
                <!-- alert -->
             <!--    <a class="dropdown-item text-center small text-gray-500" href="#">Read More Messages</a>
              </div>
            </li> -->

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo ucfirst($_SESSION['firstname']).' '.ucfirst($_SESSION['lastname']) ?></span>
                <img class="img-profile rounded-circle my_avatar img_profile" src="../webroot/img/site/<?php echo $auth['gender'].'.png' ?>">
              </a>
              <!-- Dropdown - User Information -->
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->