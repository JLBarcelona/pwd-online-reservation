<?php session_start();
  if (!isset($_SESSION['user_type']) == 3) {
    @header('location:../');
  }
   include('../config.php');
   include('../function.php');
   $id = $_SESSION['profile_id'];
   $auth = get_session($con,$id);
 ?>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <link rel="icon" type="icon/png" href="../webroot/img/site/logo2.png">
  <title class="text-capitalize"><?php echo ucfirst($auth['fname']).' '.ucfirst($auth['lname']) ?></title>

  <!-- Custom fonts for this template-->
  <link href="../webroot/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Custom styles for this template-->

  <link href="../webroot/css/admin/sb-admin-2.min.css" rel="stylesheet">

  <link href="../webroot/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="../webroot/assets/css/animate.css">
  <link rel="stylesheet" type="text/css" href="../webroot/assets/css/sweetalert.css">
 
</head>