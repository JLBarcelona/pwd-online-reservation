<?php 
session_start();
include('../config.php');
include('../function.php');


$id = $_SESSION['profile_id'];

if (!empty($_FILES['files'])) {
	$files = $_FILES['files'];

	$file_location = '../webroot/upload/'.$id;

	$is_photo = $_POST['is_photo'];

	for ($i=0; $i < count($files['name']); $i++) { 

		$file_name = $files['name'][$i];
		$file_tmp = $files['tmp_name'][$i];
		$file_type = $files['type'][$i];
		$file_size = $files['size'][$i];
		$file_error = $files['error'][$i];

		$file_ext = explode('.', $file_name);
		$extention = strtolower(end($file_ext));

		if (check_upload_2x2($con,$id) > 0 && $is_photo == 1) {
			echo 200;
		}
		else if (check_upload($con,$id) == 2 && check_upload_2x2($con,$id) == 0 && $is_photo == 0) {
			echo 202;
		}
		else if (check_upload($con,$id) >= 3) {
			// Check if you upload more than or equal to 3 files.
			echo 505;
		}else{
			if ($file_error === 0) {
				if ($file_size < 1000000) {
					if (!file_exists($file_location)) {
						mkdir($file_location);
					}

					$new_filename = uniqid('', true).'.'.$extention;
					$destination = $file_location.'/'.$new_filename;
					move_uploaded_file($file_tmp, $destination);
					
					$data = array('profile_id' => $id,'file_name' => $file_name,'file_path' => $new_filename,'file_type' =>$file_type ,'file_size' =>$file_size , 'is_photo' => $is_photo,'date_registered' => date('Y-m-d'));
					
					$sql = "INSERT INTO tbl_requirements(profile_id,file_name,file_path,file_type,file_size,is_photo,date_registered) VALUES (:profile_id,:file_name,:file_path,:file_type,:file_size,:is_photo,:date_registered)";

					echo save($con,$data,$sql);
				}
			}else{
				echo 0;
			}
		}
	}


	

	



}

 ?>