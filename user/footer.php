  <!-- Bootstrap core JavaScript-->
  <script src="../webroot/vendor/jquery/jquery.min.js"></script>
  <script src="../webroot/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../webroot/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../webroot/js/admin/sb-admin-2.min.js"></script>
  

  <script src="../webroot/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="../webroot/vendor/datatables/dataTables.bootstrap4.min.js"></script>
  <script src="../webroot/js/admin/demo/datatables-demo.js"></script>

  <script src="../webroot/js/user.js"></script>
  <script src="../webroot/js/tools.js"></script>
  <script src="../webroot/js/settings.js"></script>

  <script src="../webroot/assets/js/sweetalert.min.js"></script>