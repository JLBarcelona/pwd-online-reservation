<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>
<body id="page-top" onload="show_files();">

  <!-- Page Wrapper -->
  <div id="wrapper">

      <?php include('sidenav.php'); ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Content Row -->
           <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-12">
               <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">My Requirements</h6>
                     <div class="dropdown no-arrow">
                        <a class="dropdown-toggle btn btn-sm btn-info" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v fa-sm fa-fw text-white-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" >
                          <div class="dropdown-header">Option</div>
                          <a class="dropdown-item" href="#" data-toggle="modal" data-backdrop="static" data-target="#uploadModal" onclick="set_upload('Upload 2x2 Picture',1);"><i class="fa fa-image"></i> Upload 2x2 picture</a>

                          <a class="dropdown-item" onclick="set_upload('Upload Documents',0);" href="#" data-toggle="modal" data-backdrop="static" data-target="#uploadModal"><i class="fa fa-folder-plus"></i> Upload Documents</a>
                        </div>
                      </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="fileTable" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th class="text-truncate">#</th>
                          <th class="text-truncate">Filename</th>
                          <th class="text-truncate">Type</th>
                          <th class="text-truncate">size</th>
                          <th class="text-truncate">Date Uploaded</th>
                          <th class="text-truncate">Action</th>
                        </tr>
                      </thead>
                      <tbody id="file_data">
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  
<?php 
  include('modal.php');
  include('footer.php'); 
?>

</body>

</html>

