<!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to leave your account.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="../logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>


  <!-- Logout Modal-->
  <div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel"><span id="title_upload">Upload Documents</span></h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <form id="upload_form"  enctype="multipart/form-data">
              <input type="hidden" name="is_photo" id="is_photo" value="0">
              <div class="form-inline">
                <input type="file" name="files[]" multiple="" id="files[]" class=" mr-2 mb-2" style="border:solid 2px #c1c1c1; padding:2px; border-radius:5px;">
                <button class="btn btn-primary pull-right" id="up_btn" >Upload</button>
              </div>
            </form>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>

<script type="text/javascript">
  function set_upload(title,val){
    $("#title_upload").text(title);
    $("#is_photo").val(val);

    if (val == 1) {
      $("#files").attr('accept','image/x-png,image/jpeg');
       $("#files").removeAttr('multiple');
         $("#files").val('');
    }else{
       $("#files").attr('accept','');
       $("#files").attr('multiple', true);
         $("#files").val('');
    }
  }
</script>

   <!-- Logout Modal-->
  <div class="modal fade" id="requestModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">New Request</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
              <div class="col-xl-12 col-sm-12">
                <div class="form-group">
                  <input type="hidden" name="original_id_number" id="original_id_number" class="form-control" value="<?php echo $auth['generated_id'] ?>">

                  <!-- <label for="due_date">ID number</label> -->
                  <input type="hidden" name="id_number" id="id_number" class="form-control" value="<?php echo $auth['generated_id'] ?>" placeholder="Enter your ID number..." disabled>
                  <span id="alert_idnumber" class="text-danger"></span>
                </div>
              </div>

              <div class="col-xl-12 col-sm-12">
                <div class="form-group">
                  <label for="due_date">Schedule</label>
                  <input type="date" name="due_date" id="due_date" class="form-control">
                  <span id="alert_duedate" class="text-danger"></span>
                </div>
              </div>

              <div class="col-xl-12 col-sm-12">
                <div class="form-group">
                  <label for="due_date">Request Type</label>
                  <!-- oninput="choose_type(this.value)" -->
                  <select class="form-control" id="type_request" >
                      <option selected="" disabled="" value="">Select Request type</option>
                      <option value="New">New</option>
                      <option value="Renew">Renew</option>
                  </select>
                  <span id="alert_request" class="text-danger"></span>
                </div>
              </div>
            
            <div class="col-xl-12 col-sm-12">
              <p class="text-dark"><span class="fa-2x text-danger">Note</span>:  Please upload your supporting documents <br> <big>(Valid ID or Birthcertificate and 2x2 picture)</big> and we will review your request. </p>
              <a href="files.php" target="_blank">Upload now!</a>
            </div>
            
            </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button class="btn btn-success" onclick="make_request();"><i class="fa fa-paper-plane"></i> Send Request</button>
        </div>
      </div>
    </div>
  </div>


<script type="text/javascript">
  function choose_type(type){
    var default_val = $("#original_id_number").val();

    if (type == 'New') {
      $("#id_number").val(default_val);
      $("#id_number").attr('disabled', true);
    }else{
        $("#id_number").val('');
        $("#id_number").attr('disabled', false);
        $("#id_number").focus();
    }
  }
</script>
