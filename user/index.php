<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>
<body id="page-top" onload="show_request();">

  <!-- Page Wrapper -->
  <div id="wrapper">

      <?php include('sidenav.php'); ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Content Row -->
           <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-12">
               <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">My Request</h6>
                   <a href="#" role="button" id="dropdownMenuLink" data-toggle="modal" data-backdrop="static" data-target="#requestModal" class="btn btn-success btn-sm" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-plus fa-sm fa-fw"></i>
                    </a>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-bordered" id="request_table" width="100%" cellspacing="0">
                      <thead>
                        <tr>
                          <th class="text-truncate">#</th>
                          <th class="text-truncate">Request</th>
                          <th class="text-truncate">Date Request</th>
                          <th class="text-truncate">Due Date</th>
                          <th class="text-truncate">Status</th>
                          <th class="text-truncate">Action</th>
                        </tr>
                      </thead>
                      <tbody id="request_data">
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  
<?php 
  include('modal.php');
  include('footer.php'); 
?>

</body>

</html>

