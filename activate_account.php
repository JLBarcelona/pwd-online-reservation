<?php session_start();
if (isset($_SESSION['user_type']) == 3) {
  @header('location:user/');
}else if (isset($_SESSION['user_type']) == 2) {
  @header('location:staff/');
}else if (isset($_SESSION['user_type']) == 1) {
  @header('location:admin');
}
include("config.php");
include("function.php");
 ?>
<?php  ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>PWD Online Appointment</title>


  <link rel="icon" type="icon/png" href="webroot/img/site/logo2.png">
  <!-- Custom fonts for this theme -->
  <link href="webroot/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="webroot/assets/css/animate.css">
  <link rel="stylesheet" type="text/css" href="webroot/assets/css/sweetalert.css">
  <!-- Theme CSS -->
  <link href="webroot/css/freelancer.css" rel="stylesheet">
</head> 

 <style type="text/css">
    .mt {
      margin-top: 60px;
    }
    @media only screen and (max-width: 600px) {
     .mt {
      margin-top: 90px;
    }
    .header-text{
      display: none;
    }
    }
</style>


<body id="page-top" style="background-color: #ccfff8">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top navbar-shrink"  id="mainNav">
    <div class="container-fluid p-0 m-0">
      <a class="navbar-brand js-scroll-trigger" href="#page-top" id="text-head"><i class="fa fa-wheelchair fa-2x bg-primary p-2 rounded" style="background-color: #165296 !important; border: solid 1px #fff;"></i> <span class="header-text">PWD's ID ONLINE APPOINTMENT RESERVATION</span></a>
      <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
    
    </div>
  </nav>

  <!-- Portfolio Section -->
  <section class="page-section portfolio " id="portfolio" style="background-color: #ccfff8">
    <div class="container">

      <!-- Portfolio Section Heading -->
      <h3 class="text-center text-uppercase text-secondary mb-0">Forgot Password ?</h3>

      <!-- Icon Divider -->
      <div class="divider-custom">
        <div class="divider-custom-line"></div>
        <div class="divider-custom-icon">
          <i class="fas fa-question-circle"></i>
        </div>
        <div class="divider-custom-line"></div>
      </div>

      <!-- Portfolio Grid Items -->
      <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
           <div class="card">
            <div class="card-body login-card-body">
              <form action="#" method="post" id="formemail">
                <div class="input-group mb-3">
                  <input type="text" class="form-control" placeholder="Email" id="email" autocomplete="off">
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-envelope"></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-8">
                  </div>
                  <!-- /.col -->
                  <div class="col-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                  </div>
                  <!-- /.col -->
                </div>
              </form>


              <form action="#" method="post" id="form_code" style="display: none;">
                <div class="input-group mb-3">
                  <input type="password" class="form-control" placeholder="Verification Code" id="code" autocomplete="off">
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-code"></span>
                    </div>
                  </div>
                </div>
                <hr>

                <div class="input-group mb-3">
                  <input type="password" class="form-control" placeholder="New Password" id="new_password" autocomplete="off">
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-lock"></span>
                    </div>
                  </div>
                </div>

                <div class="input-group mb-3">
                  <input type="password" class="form-control" placeholder="Confirm Password" id="c_password" autocomplete="off">
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-lock"></span>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-8">
                  </div>
                  <!-- /.col -->
                  <div class="col-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                  </div>
                  <!-- /.col -->
                </div>
              </form>

              <!-- /.social-auth-links -->
          
            </div>
            <!-- /.login-card-body -->
          </div>
        </div>
        <div class="col-sm-4"></div>
      </div>
      <!-- /.row -->

    </div>
  </section>


  <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
  <div class="scroll-to-top d-lg-none position-fixed ">
    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
      <i class="fa fa-chevron-up"></i>
    </a>
  </div>


  <!-- Bootstrap core JavaScript -->
  <script src="webroot/vendor/jquery/jquery.min.js"></script>
  <script src="webroot/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="webroot/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Contact Form JavaScript -->
  <script src="webroot/js/jqBootstrapValidation.js"></script>
  <script src="webroot/js/contact_me.js"></script>

  <!-- Custom scripts for this template -->
  <script src="webroot/js/freelancer.js"></script>
  <script src="webroot/js/index.js"></script>
  <script src="webroot/js/tools.js"></script>
  <script src="webroot/assets/js/sweetalert.min.js"></script>

</body>

</html>
<script type="text/javascript">
  
$("#formemail").on('submit', function(e){
  e.preventDefault();
  var email = $("#email");

  if (email.val() == "") {
    email.focus();
    swal("Oops!","email is required!","error");
  }
  else{ 
    var mydata = 'action=request_code'+'&email=' + email.val();
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
       swal("Loading","Please Wait...","info");
      },
      success:function(data){
        console.log(data.trim());
        if (data.trim() == 1) {
          $("#formemail").hide('fast');
          $("#form_code").show('fast');
        swal("Loading","You will receive your verification code in your email!","success");
          // window.location="./";
        }else{
        swal("Oops!","email address is not exist!","error");
          console.log(data.trim());
        }
      }
    });
  }

  });



$("#form_code").on('submit', function(e){
  e.preventDefault();
  
  var code = $("#code");
  var new_password = $("#new_password");
  var c_password = $("#c_password");
  
  if (code.val() == "") {
    code.focus();
    swal("Oops!","Verification code is required!","error");
  }
  else if (new_password.val() == "") {
    new_password.focus();
    swal("Oops!","New password is required!","error");
  }
  else if (c_password.val() == "") {
    c_password.focus();
    swal("Oops!","Confirm your password!","error");
  }
  else if (c_password.val() != new_password.val()) {
    c_password.focus();
    swal("Oops!","Pasword doesn\'t match!","error");
  }
  else{ 
    var mydata = 'action=forgot_password'+'&code=' + code.val() + '&new_password=' + new_password.val();
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
       swal("Loading!","Please Wait...","info");
      },
      success:function(data){
        console.log(data.trim());
        if (data.trim() == 1) {a
        swal("Success","Password has been updated!","success");
           setTimeout(function(){
             window.location="./";
           },1500);
        }else{
        swal("Oops!","Invalid Code!","error");
          console.log(data.trim());
        }
      }
    });
  }

  });
</script>