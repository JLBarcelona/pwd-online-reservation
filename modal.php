 <!-- Portfolio Modal 1 -->
  <div class="portfolio-modal modal fade animated slideInDown" id="login" tabindex="-1" role="dialog" aria-labelledby="portfolioModal1Label" aria-hidden="true">
    <div class="modal-dialog " role="document">
      <div class="modal-content">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            <i class="fas fa-times fa-1x"></i>
          </span>
        </button>
        <div class="modal-body ">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-lg-12">
                <div class="text-center">
                  <!-- Portfolio Modal - Title -->
                  <h4 class=" text-secondary text-uppercase mb-0">Log in</h4>
                  <!-- Icon Divider -->
                  <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon">
                      <i class="fas fa-user-lock"></i>
                    </div>
                    <div class="divider-custom-line"></div>
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-sm-12 form-group">
                    <h5 class="text-dark">E-mail</h5>
                    <input type="text" name="email_add" id="email_add" class="form-control form-control-lg" placeholder="Enter e-mail address...">
                    <span class="text-danger" id="alert_email_add"></span>
                  </div>

                   <div class="col-sm-12 form-group">
                    <h5 class="text-dark">Password</h5>
                    <input type="password" name="pwd" id="pwd" class="form-control form-control-lg" placeholder="Enter password...">
                    <span class="text-danger" id="alert_pwd"></span>
                  </div>

                    <div class="col-sm-12 form-group text-right">
                       <a href="forget_password.php" class="btn btn-link btn-lg">Forgot Password?</a>
                      <button class="btn btn-primary btn-lg" id="btn_log" onclick="login()">Login</button>
                    </div>

                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
