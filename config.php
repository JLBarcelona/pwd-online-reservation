<?php 
$servername = 'localhost';
$username = 'root';
$password = '';

try {
	$ATTR = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC];
	// quotation always double
	$con = new PDO("mysql:host=$servername;dbname=db_pwd", $username, $password, $ATTR);
	// echo 'connected';
} catch (PDOException $e) {
	 echo "Connection failed: " . $e->getMessage();
}