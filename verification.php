<?php session_start();
if (isset($_SESSION['user_type']) == 3) {
  @header('location:user/');
}else if (isset($_SESSION['user_type']) == 2) {
  @header('location:staff/');
}else if (isset($_SESSION['user_type']) == 1) {
  @header('location:admin');
}
include("config.php");
include("function.php");
 ?>
<?php  ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>PWD Online Appointment</title>


  <link rel="icon" type="icon/png" href="webroot/img/site/logo2.png">
  <!-- Custom fonts for this theme -->
  <link href="webroot/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="webroot/assets/css/animate.css">
  <link rel="stylesheet" type="text/css" href="webroot/assets/css/sweetalert.css">
  <!-- Theme CSS -->
  <link href="webroot/css/freelancer.css" rel="stylesheet">
</head> 

 <style type="text/css">
    .mt {
      margin-top: 60px;
    }
    @media only screen and (max-width: 600px) {
     .mt {
      margin-top: 90px;
    }
    .header-text{
      display: none;
    }
    }
</style>


<body id="page-top" style="background-color: #ccfff8">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top navbar-shrink"  id="mainNav">
    <div class="container-fluid p-0 m-0">
      <a class="navbar-brand js-scroll-trigger" href="#page-top" id="text-head"><i class="fa fa-wheelchair fa-2x bg-primary p-2 rounded" style="background-color: #165296 !important; border: solid 1px #fff;"></i> <span class="header-text">PWD's ID ONLINE APPOINTMENT RESERVATION</span></a>
      <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
    
    </div>
  </nav>

  <!-- Portfolio Section -->
  <section class="page-section portfolio " id="portfolio" style="background-color: #ccfff8">
    <div class="container">

      <!-- Portfolio Section Heading -->
      <h3 class="text-center text-uppercase text-secondary mb-0">Verify Account</h3>

      <!-- Icon Divider -->
      <div class="divider-custom">
        <div class="divider-custom-line"></div>
        <div class="divider-custom-icon">
          <i class="fas fa-question-circle"></i>
        </div>
        <div class="divider-custom-line"></div>
      </div>

      <!-- Portfolio Grid Items -->
      <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
           <div class="card">
            <div class="card-body login-card-body">
             
                <div class="form-group mb-3">
                  <input type="text" class="form-control" placeholder="Verification Code" id="ver_code" autocomplete="off">
                </div>
                <div class="row">
                  <div class="col-8">
                  </div>
                  <!-- /.col -->
                  <div class="col-4">
                    <button onclick="submit_verify_code();" class="btn btn-primary btn-block btn-flat">Submit</button>
                  </div>
                  <!-- /.col -->
                </div>
              <!-- /.social-auth-links -->
          
            </div>
            <!-- /.login-card-body -->
          </div>
        </div>
        <div class="col-sm-4"></div>
      </div>
      <!-- /.row -->

    </div>
  </section>


  <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
  <div class="scroll-to-top d-lg-none position-fixed ">
    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
      <i class="fa fa-chevron-up"></i>
    </a>
  </div>


  <!-- Bootstrap core JavaScript -->
  <script src="webroot/vendor/jquery/jquery.min.js"></script>
  <script src="webroot/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="webroot/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Contact Form JavaScript -->
  <script src="webroot/js/jqBootstrapValidation.js"></script>
  <script src="webroot/js/contact_me.js"></script>

  <!-- Custom scripts for this template -->
  <script src="webroot/js/freelancer.js"></script>
  <script src="webroot/js/index.js"></script>
  <script src="webroot/js/tools.js"></script>
  <script src="webroot/assets/js/sweetalert.min.js"></script>

</body>

</html>
<script type="text/javascript">
  function submit_verify_code(){
    var ver_code = $("#ver_code");
    // alert();
       if (ver_code.val() == "") {
        ver_code.focus();
        swal("Sorry!","Please enter your verification code!","error");
      }else{

        var mydata = 'action=confirm_account&tokens=' + ver_code.val();
        // alert(mydata);

        $.ajax({
           type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
                console.log(data.trim());
              if (data.trim() == 1) {
                swal("Success","Your account has been verified!","success");
                setTimeout(function(){
                  // window.close();
                  window.location = "./";
                },2000);
              }else{
                swal("Sorry!","Verification Code is invalid","error");
              }
            }
        });
      }
  }
</script>