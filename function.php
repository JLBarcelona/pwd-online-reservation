<?php 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require 'vendor/vendor/autoload.php';

$id = (isset($_SESSION['profile_id'])) ? $_SESSION['profile_id'] : '';

$auth = get_session($con,$id);

$able = '';

if (isset($_SESSION['profile_id'])) {
	$able = ($auth['user_type'] == 2)? 'none' : ''; 
}

function save($con,$data,$sql){
	// sample
	// INSERT INTO tbl_employee (firstname,lastname) VALUES(:fn,:ln)
	$prep = $con->prepare($sql);
	$prep->execute($data);

	if ($prep) {
		return 1;
	}
}

function get_last_id($con){
	$data = array();
	$sql ="SELECT max(profile_id) from tbl_profile";

	return max_record($con,$data,$sql,'profile_id') + 1;
}

function generate_id($id){
	$id_gen = '';


	if (strlen($id) == 2) {
		$id_gen = $id.'-'.rand(1000,9000).'-'.rand(100,900).'-'.rand(1000,9000);
	}else if (strlen($id) == 3) {
		$id_gen = rand(10,90).'-'.rand(1000,9000).'-'.$id.'-'.rand(1000,9000);
	}else if (strlen($id) == 4) {
		$id_gen = rand(10,90).'-'.$id.'-'.rand(100,900).'-'.rand(1000,9000);
	}

	return $id_gen;
}

function count_record($con,$data,$sql,$count){
    $result = fetch_record($con,$data,$sql);
    $row = $result->fetch();

    if ($row['count('.$count.')'] > 0) {
    	return $row['count('.$count.')'];
    }else{
    	return 0;
    }
}

function max_record($con,$data,$sql,$count){
    $result = fetch_record($con,$data,$sql);
    $row = $result->fetch();

    if ($row['max('.$count.')'] > 0) {
    	return $row['max('.$count.')'];
    }else{
    	return 0;
    }
}



function count_users($con,$param){
	switch ($param) {
		case 'pwds':
			$data = array();
			$sql = "SELECT a.*,b.*,count(a.profile_id) from tbl_account a left join tbl_profile b on a.profile_id=b.profile_id where a.user_type = 3";
			return count_record($con,$data,$sql,'a.profile_id');
		break;
		
		case 'staff':
			$data = array();
			$sql = "SELECT a.*,count(b.profile_id),b.* from tbl_account a left join tbl_profile b on a.profile_id=b.profile_id where a.user_type = 2";
			return count_record($con,$data,$sql,'b.profile_id');
		break;

		case 'pending':
			$data = array();
			$sql = "SELECT a.*,b.*,count(a.profile_id) from tbl_request a left join tbl_account b on a.profile_id=b.profile_id where a.approved = 0 and b.user_type=3";
			return count_record($con,$data,$sql,'a.profile_id');
		break;
		
		case 'approve':
			$data = array();
			$sql = "SELECT a.*,b.*,count(a.profile_id) from tbl_request a left join tbl_account b on a.profile_id=b.profile_id where a.approved = 1 and b.user_type=3";
			return count_record($con,$data,$sql,'a.profile_id');
		break;

		case 'disapprove':
			$data = array();
			$sql = "SELECT a.*,b.*,count(a.profile_id) from tbl_request a left join tbl_account b on a.profile_id=b.profile_id where a.approved = 3 and b.user_type=3";
			return count_record($con,$data,$sql,'a.profile_id');
		break;

		case 'claim':
			$data = array();
			$sql = "SELECT a.*,b.*,count(a.profile_id) from tbl_request a left join tbl_account b on a.profile_id=b.profile_id where a.approved = 2 and b.user_type=3";
			return count_record($con,$data,$sql,'a.profile_id');
		break;
	}
}


function get_session($con,$id){
	$data = array('id' => $id);
    $sql = "SELECT a.*,b.* from tbl_profile a left join tbl_account b on a.profile_id=b.profile_id where a.profile_id = :id";
    $result = fetch_record($con,$data,$sql);
    $row = $result->fetch();

    return $row;
}

function fetch_record($con,$data,$sql){
	// sample
	// INSERT INTO tbl_employee (firstname,lastname) VALUES(:fn,:ln)
	$prep = $con->prepare($sql);
	$prep->execute($data);

	return $prep;
}

function iteration($data){
	if (!empty($data)) {
		return $data;
	}else{
		return '';
	}
}


function check_upload($con,$id){
	$data = array('id' => $id);

	$sql = "SELECT profile_id from tbl_requirements where profile_id = :id";
	
	$prep = $con->prepare($sql);
	$prep->execute($data);

	$row = $prep->fetch();

	return $prep->rowCount();
}

// function check_profile_picture($con,$gender,$photo,$count){
// 	if ($count > 0) {
// 		return $photo;
// 	}else{
// 		return $gender;
// 	}
// }

function check_upload_2x2($con,$id){
	$data = array('id' => $id);

	$sql = "SELECT profile_id from tbl_requirements where profile_id = :id and is_photo = 1";
	
	$prep = $con->prepare($sql);
	$prep->execute($data);

	$row = $prep->fetch();

	return $prep->rowCount();
}


function verify_record($con,$data,$sql){
	$prep = $con->prepare($sql);
	$prep->execute($data);

	$row = $prep->fetch();

	return $prep->rowCount();
}


function request_status($status){
	if ($status == 0) {
		return 'Pending';
	}else if ($status == 1) {
		return 'Approved';
	}else if ($status == 2) {
		return 'Claimed';
	}else if ($status == 3) {
		return 'Disapprove';
	}else if ($status == 4) {
		return 'Finished';
	}
}


function userAuth($row){
	$_SESSION['account_id'] = iteration($row['account_id']);
	$_SESSION['profile_id'] = iteration($row['profile_id']);
	$_SESSION['fullname'] = iteration($row['fname'].' '.$row['lname']);
	$_SESSION['firstname'] = iteration($row['fname']);
	$_SESSION['middlename'] = iteration($row['mname']);
	$_SESSION['lastname'] = iteration($row['lname']);
	$_SESSION['birthdate'] = iteration($row['birthdate']);	
	$_SESSION['gender'] = iteration($row['gender']);
	$_SESSION['address'] = iteration($row['address']);
	$_SESSION['disability_type'] = iteration($row['disability_type']);
	$_SESSION['guardian_name'] = iteration($row['guardian_name']);
	$_SESSION['guardian_number'] = iteration($row['guardian_number']);
	$_SESSION['profile_type'] = iteration($row['profile_type']);

	// user types 1 = admin, 2 = staff and 3 = pwd_user
	$_SESSION['user_type'] = iteration($row['user_type']);
	// verifications puposes
	$_SESSION['email'] = iteration($row['email_address']);
	$_SESSION['password'] = iteration($row['password']);
}


function logs($con,$messages,$id){
	$data = array('profile_id' => $id, 'message' => $messages, 'date_reg' => date('Y-m-d'));
	$sql = "INSERT INTO tbl_log (profile_id,message,date_register) VALUES (:profile_id,:message,:date_reg)";
	$prep = $con->prepare($sql);
	$prep->execute($data);

	if ($prep) {
		return 1;
	}
}

function register_account($con,$profile_id,$email,$password_hashed,$access_token,$approve,$status){
	

	$data2 = array('profile_id' => $profile_id,'email' => $email, 'password' => $password_hashed ,'access_token' => $access_token,'user_type' => $status,'date_reg' => date('Y-m-d'), 'approval' => $approve);

	 $query = "INSERT INTO tbl_account (profile_id,email_address,password,access_token,user_type,date_registered,is_approve) VALUES (:profile_id,:email,:password,:access_token,:user_type,:date_reg,:approval);";

	 $result = save($con,$data2,$query);

	 return $result;
}


function GetLastId($con,$data,$sql){
	// sample
	// INSERT INTO tbl_employee (firstname,lastname) VALUES(:fn,:ln)
	$prep = $con->prepare($sql);
	$prep->execute($data);

	return $con->lastInsertId();
}


function delete_query($con,$data,$sql){
	// sample
	// DELETE from tbl_employee WHERE id=:id
	$prep = $con->prepare($sql);
	$prep->execute($data);

	if ($prep) {
		return 1;
	}
}


function send_email($name,$email,$link){
	// Instantiation and passing `true` enables exceptions
	$mail = new PHPMailer(true);

	try {

	    //Server settings
     	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->SMTPDebug = false; 								  // Enable verbose debug output
	    $mail->isSMTP(); 									  // Set mailer to use SMTP
	    $mail->Host = 'smtp.gmail.com';  				      // Specify main and backup SMTP servers
	    $mail->SMTPAuth = true; 							  // Enable SMTP authentication
	    $mail->Username = 'hermo12345six@gmail.com'; 				  // SMTP username
	    $mail->Password = 'greatwhite18!';                           // SMTP password
	    $mail->Port = 587;                                    // TCP port to connect to

	    //Recipients
	    $mail->setFrom('hermo12345six@gmail.com', 'PWD\'s Online Appointment.');
	    $mail->addAddress($email, $name);     // Add a recipient
	 
	    $messages = 'Hi,'.$name.' !<br><br><br>';
	    $messages .= 'Thank you for Registering to our system.<br><br>';
	    $messages .= 'Please copy the link bellow and paste to your web browser to verify your account <br><br><br>'.$link.'.';

	    // Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = '(PWD\'s Online Appointment Reservation) Please Verify your account!';
	    $mail->Body = $messages;
	    $mail->AltBody = '';

		if ($mail->send()) {
			echo  1;
		}

	} catch (Exception $e) {
	    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
	}
}



function send_email_all($name,$email,$code){
	// Instantiation and passing `true` enables exceptions
	$mail = new PHPMailer(true);

	try {
	    //Server settings
     	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	    $mail->SMTPDebug = false; 								  // Enable verbose debug output
	    $mail->isSMTP(); 									  // Set mailer to use SMTP
	    $mail->Host = 'smtp.gmail.com';  				      // Specify main and backup SMTP servers
	    $mail->SMTPAuth = true; 							  // Enable SMTP authentication
        $mail->Username = 'hermo12345six@gmail.com'; 				  // SMTP username
	    $mail->Password = 'greatwhite18!';                           // SMTP password
	    $mail->Port = 587;                                    // TCP port to connect to

	    //Recipients
	    $mail->setFrom('hermo12345six@gmail.com', 'PWD\'s Online Appointment.');
	    $mail->addAddress($email, $name);     // Add a recipient
	 
	    $messages = 'Hello! '.$name.'.<br>';
	    $messages .= 'Please Paste your verification code from forgot password form.<br><br>';
	    $messages .= '********************<br>';
	    $messages .= 'Your Code is:'.$code.'<br>';
	    $messages .= '********************<br>';

	    // Content
	    $mail->isHTML(true);                                  // Set email format to HTML
	    $mail->Subject = '(PWD\'s Online Appointment Reservation) Please Verify your account!';
	    $mail->Body = $messages;
	    $mail->AltBody = '';

		if ($mail->send()) {
			echo  1;
		}

	} catch (Exception $e) {
	    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
	}
}


function select_address(){
	?>
	<option value="Abra">Abra, Santiago City</option>
	<option value="Ambalatungan">Ambalatungan, Santiago City</option>
	<option value="Balintocatoc">Balintocatoc, Santiago City</option>
	<option value="Baluarte">Baluarte, Santiago City</option>
	<option value="Bannawag Norte">Bannawag Norte, Santiago City</option>
	<option value="Batal">Batal, Santiago City</option>
	<option value="Buenavista">Buenavista, Santiago City</option>
	<option value="Cabulay">Cabulay, Santiago City</option>
	<option value="Calao East">Calao East, Santiago City</option>
	<option value="Calao West">Calao West, Santiago City</option>
	<option value="Calaocan">Calaocan, Santiago City</option>
	<option value="Villa Gonzaga">Villa Gonzaga, Santiago City</option>
	<option value="Centro East">Centro East, Santiago City</option>
	<option value="Centro West">Centro West, Santiago City</option>
	<option value="Divisoria ">Divisoria , Santiago City</option>
	<option value="Dubinan East">Dubinan East, Santiago City</option>
	<option value="Dubinan West">Dubinan West, Santiago City</option>
	<option value="Luna">Luna, Santiago City</option>
	<option value="Mabini">Mabini, Santiago City</option>
	<option value="Malvar">Malvar, Santiago City</option>
	<option value="Nabbuan">Nabbuan, Santiago City</option>
	<option value="Naggasican">Naggasican, Santiago City</option>
	<option value="Patul">Patul, Santiago City</option>
	<option value="Plaridel">Plaridel, Santiago City</option>
	<option value="Rizal">Rizal, Santiago City</option>
	<option value="Rosario">Rosario, Santiago City</option>
	<option value="Sagana">Sagana, Santiago City</option>
	<option value="Salvador">Salvador, Santiago City</option>
	<option value="San Andres">San Andres, Santiago City</option>
	<option value="San Isidro">San Isidro, Santiago City</option>
	<option value="San Jose">San Jose, Santiago City</option>
	<option value="Sinili">Sinili, Santiago City</option>
	<option value="Sinsayon">Sinsayon, Santiago City</option>
	<option value="Santa Rosa">Santa Rosa, Santiago City</option>
	<option value="Victory Norte">Victory Norte, Santiago City</option>
	<option value="Victory Sur">Victory Sur, Santiago City</option>
	<option value="Villasis">Villasis, Santiago City</option>
	<?php
}

?>



