<?php 
session_start();
include("config.php");
include("function.php");
include("sms_helper.php");
// include("helper.php");

$id_profile = $auth['profile_id'];
$name_log = ucfirst($auth['fname']).', '.ucfirst($auth['lname']);



$action =  $_POST['action'];
switch ($action) {
	case 'register':
		$fn = $_POST['fn'];
		$mn = $_POST['mn'];
		$ln = $_POST['ln'];
		$gender = $_POST['gender'];
		$cp_number = $_POST['cp_number'];
		$disability = $_POST['disability'];
		$address = $_POST['address'];
		$email = $_POST['email'];
		$gname = $_POST['gname'];
		$contacts = $_POST['contacts'];
		$birthdate = $_POST['bdate'];
		$password = $_POST['password'];
		$blood_type = $_POST['blood_type'];

		$data_verify = array('emails' => $email);

		$verify = "SELECT * from tbl_account Where email_address = :emails";
	    
		$prep = $con->prepare($verify);
		$prep->execute($data_verify);

		 if ($prep->rowCount() > 0) {
			echo 2;
		 }else{
 		 	 // Generate ID number
 		 	 $max_id = get_last_id($con);
		 	 $generate_id = generate_id($max_id);
		 	 // Generate ID number

		 	 $data = array('gen_id' => $generate_id, 'fn' => $fn, 'mn' => $mn, 'ln' => $ln,'gender' => $gender, 'contact_number' => $cp_number,'blood_type' => $blood_type,'disability' => $disability,'address' => $address,'gname' => $gname,'contacts' => $contacts,'date_reg' => date('Y-m-d'), 'birthdate' => $birthdate);
			 $sql = "INSERT INTO tbl_profile (generated_id,fname,mname,lname,birthdate,gender,contact_number,blood_type,address,disability_type,guardian_name,guardian_number,date_registered) VALUES(:gen_id,:fn,:mn,:ln,:birthdate,:gender,:contact_number,:blood_type,:address,:disability,:gname,:contacts,:date_reg)";

			 
			 // get last id from profile.
			 $profile_id = GetLastId($con,$data,$sql);

			  $seed = str_split('abcdefghijklmnopqrstuvwxyz'.'ABCDEFGHIJKLMNOPQRSTUVWXYZ'); // and any other characters
	     	  shuffle($seed); // probably optional since array_is randomized; this may be redundant
	    	  $rand = '';
	     	  foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];

	     	  $code = $rand.$profile_id;

			 // Create access token for verification
			 $token = $code;
			 $message = '';
			 $message .= "PWD's Online Reservation\n";
			 $message .= 'Hello, '.ucfirst($fn).' '.ucfirst($ln).'!';
			 $message .= "\nYour Account Verification code is (".$token.")\n\n";

			 $access_token = password_hash($token, PASSWORD_DEFAULT);

			 $password_hashed = password_hash($password, PASSWORD_BCRYPT);


			 $new_hash = str_replace("$2y$", "$2a$", $password_hashed);

			 $result = register_account($con,$profile_id,$email,$new_hash,$access_token,0,3);

			 // Make verification link
			 $link = 'localhost/PWD_online_reservation/verify.php?API_token='.$access_token.'&id='.$profile_id;
			 // send_email($fn,$email,$link)
			 if ($result == 1) {
			 	if (send_sms_now($cp_number,$message) > 0) {
			 		echo $id;
			 	}
			 }
		 }
	break;

	case 'execute_db':
		$data = array();
		$file_path = $_POST['file_path'];
		$file_data = file($file_path);
		$output = '';
		$count = 0;


		foreach ($file_data as $row) {
			$start_char = substr(trim($row), 0, 2);
			if ($start_char != '--' || $start_char != '/*' || $start_char != '//' || $start_char != '') {
				$output = $output . $row; 
				$end_char = substr(trim($row), -1, 1);
				if ($end_char == ';') {	
					if (save($con,$data,$output) != 1) {
						$count++;
					}
				}
			}
		}

		if ($count > 0) {

			logs($con,$name_log.' has restore the database.',$id_profile);
			echo $count;
		}else{
			echo 0;
		}

	break;

	case 'backup_db':
		$db_name = 'db_pwd';
		$show_data = array();
		$sql_tbl = "SHOW TABLES";
		$result = fetch_record($con,$show_data,$sql_tbl);

		$output = "SET FOREIGN_KEY_CHECKS=0;";

		while ($tbls = $result->fetch()) {
			$tables = $tbls['Tables_in_'.$db_name];

			$data =array();
			$sql = "SHOW CREATE TABLE ".$tables."";
			$res = fetch_record($con,$data,$sql);
			$row = $res->fetch();

			 $output .= "DROP TABLE IF EXISTS `".$tables."`;";
			 $output .= "". $row['Create Table'] .";";
			 // echo $output;

			 $data_record = array();
			 $sql_record = "SELECT * from ".$tables."";
			 $data_result = fetch_record($con,$data_record,$sql_record);
			
			 while ($datas = $data_result->fetch()) {
			 	 $tbl_collumn = array_keys($datas);
				 $tbl_data = array_values($datas);

				 // echo $tbl_data[1];
				 // var_dump($tbl_data);
				 $str = implode("','", $tbl_data);
				 $output .= "INSERT INTO $tables (";
				 $output .= "". implode(', ', $tbl_collumn) .") VALUES (";
				 $output .= "'".str_replace("''", 'null', $str)."');";
			 }
			
		}
		// echo $output;
		$file_directory = 'database_backup';
		$filename = $file_directory."/db_pwd_".date('Y-m-d').'.sql';
		$file_handle = fopen($filename, 'w');
		fwrite($file_handle, $output);
		if (fclose($file_handle)) {
			logs($con,$name_log.' has created database backup.',$id_profile);
			echo 1;
		}
	break;

	case 'database_files':
		$file_directory = 'database_backup';
			$file =	scandir($file_directory);
			foreach ($file as $key => $value) {
				if ($key != 0 && $key != 1) {
					?>
					<tr>
						<td><?php echo $value ?></td>
						<td class="text-center">
							<button class="btn btn-dark" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
							<div class="dropdown-menu">
								<a download="" href="<?php echo '../'.$file_directory.'/'.$value ?>" class="dropdown-item"><i class="fa fa-download text-primary"></i> Download</a>

								<a href="#" class="dropdown-item" onclick="execute_backup('<?php echo $value ?>','<?php echo $file_directory.'/'.$value ?>');"><i class="fa fa-recycle text-success"></i> Restore</a>
								<!-- <a href="cores/delete.php?url=<?php echo '../'.$file_directory.'/'.$value ?>" class="dropdown-item">
									<i class="fa fa-trash text-danger"></i> Delete</a> -->
							</div>
							
						</td>
					</tr>
					<?php
				}
			}
		break;


	case 'send_all_inbox':
		$dd = array();
		$ss = "SELECT a.*,b.* from tbl_profile a left join tbl_request b on a.profile_id=b.profile_id where a.date_deleted is null and b.approved = 4";

		$rr = fetch_record($con,$dd,$ss);
		$count = 0;
		$message = '';
		$status = false;


		while ($row = $rr->fetch()) {
			
			 $name = $row['fname'].' '.$row['lname'];
			 $cp_number = $row['contact_number'];
			 $id_number = $row['generated_id'];

			 $message .= "PWD's Online Reservation\n";
			 $message .= "Hello, ".ucfirst($name)."!\n";
			 $message .= 'Please Present your ID number to claim ('.$id_number.')';
			 $message .= "\nPlease Claim now your PWD ID!\n\n";
			
			if ($msg = send_sms_now($cp_number,$message) > 0) {
				$status = true;
			}else{
				$count++;
			}

			if ($status) {
				echo 1;
				logs($con,$name_log.' has sent sms Notification for ID claiming.',$id_profile);

			}
		}
	break;

	case 'confirm_account':
		$tokens = $_POST['tokens'];
		$token = '';
		$id = '';
		$result = 0;
		$dd = array();
		$ss = "SELECT * from tbl_account where date_deleted is null and is_approve = 0";

		$rr = fetch_record($con,$dd,$ss);

		while ($rows = $rr->fetch()) {
			// echo $rows['access_token'];

			// echo $tokens;
			if (password_verify($tokens, $rows['access_token']) == true) {
				$token = $rows['access_token'];
				$id = $rows['profile_id'];

				$data = array('id' => $id, 'approve' => 1);
				$sql = "UPDATE tbl_account set is_approve = :approve where profile_id = :id";
				$result = save($con,$data,$sql);
			}
		}

		if ($result > 0) {
			echo 1;
		}

		

	 break;

	case 'forgot_password':

		$code = $_POST['code'];
		$pwd = $_POST['new_password'];

		$datas = array('code' => $code);
		$sqls = "SELECT * from tbl_account where verification_code=:code";

		if (verify_record($con,$datas,$sqls) > 0) {

			$result = fetch_record($con,$datas,$sqls);

			$row = $result->fetch();

			$new_password = password_hash($pwd, PASSWORD_DEFAULT);
			$data = array('password' => $new_password, 'id' => $row['account_id']);
			$sql = "UPDATE tbl_account set password=:password where account_id=:id";

			if (save($con,$data,$sql) > 0) {
				echo 1;
				logs($con,'has Changed forgot password.',$row['profile_id']);

			}
		}else{
			echo 0;
		}

		

	break;

	case 'request_code':
		$email = $_POST['email'];
		$code ='';


		$data = array('email' => $email);		
		$sql = "SELECT a.*,b.* from tbl_account a left join tbl_profile b on a.profile_id=b.profile_id   where a.email_address=:email";

		if (verify_record($con,$data,$sql) > 0) {
			$result = fetch_record($con,$data,$sql);

		   $row = $result->fetch();

		   $name = $row['fname'].' '.$row['lname']; 

		   $cp_number = $row['contact_number'];


		   $seed = str_split('abcdefghijklmnopqrstuvwxyz'.'ABCDEFGHIJKLMNOPQRSTUVWXYZ'); // and any other characters
     	   shuffle($seed); // probably optional since array_is randomized; this may be redundant
    	   $rand = '';
     	   foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];

      	    $code = $rand.$row['account_id'];

	  	     $message = '';
			 $message .= "PWD's Online Reservation\n";
			 $message .= 'Hello, '.ucfirst($name).'!';
			 $message .= "\nForgot Password Verification Code (".$code.")\n\n";




			$datas = array('user_id' => $row['account_id'],'code' => $code);
			$query = "UPDATE tbl_account set verification_code=:code where account_id=:user_id";
			if (save($con,$datas,$query) > 0) {
				if (send_sms_now($cp_number,$message) > 0) {
					// echo 1;
					send_email_all($name,$email,$code);
					logs($con,$name.' has requesting verification code for Forgotten password.',$row['profile_id']);
					echo 1;
				}
			}
		}else{
			echo 0;
		}

	break;

	case 'login':
		$email_add = $_POST['email_add'];
		$pwd = $_POST['pwd'];

		$data = array('email' => $email_add);

		$sql = "SELECT a.*,b.* from tbl_profile a left join tbl_account b on a.profile_id=b.profile_id where b.email_address = :email and is_approve = 1 and b.date_deleted is null";
		// $name = $row['fname'].' '.$row['lname']; 
		$prep = $con->prepare($sql);
		$prep->execute($data);

		if ($prep->rowCount() > 0) {
			$row = $prep->fetch();
			$name = $row['fname'].' '.$row['lname']; 
			if (password_verify($pwd, $row['password'])) {
				userAuth($row);
				echo $row['user_type'];
				logs($con,$name.' has login.',$row['profile_id']);

			}else{
				echo 0;
			}

		}else{
			echo 0;
		}

	break;

	// Users
	case 'file_manager':
		$data = array('id' => $id);
		$sql ="SELECT * from tbl_requirements where profile_id = :id";

		$prep = $con->prepare($sql);
		$prep->execute($data);

		$path = '../webroot/upload/'.$id.'/';
		$i = 0;
		while($row = $prep->fetch()){
		$i++;
		?>
		<tr>
	      <td class="text-truncate"><?php echo $i ?></td>
	      <td class=""><?php echo $row['file_name'] ?></td>
	      <td class="text-truncate"><?php echo $row['file_type'] ?></td>
	      <td class="text-truncate"><?php echo $row['file_size'].'KB' ?></td>
	      <td class="text-truncate" ><?php echo $row['date_registered'] ?></td>
	      <td class="text-center">
	      	<button class="btn btn-dark dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
	      	<div class="dropdown-menu">
	      		<a href="<?php echo $path.$row['file_path'] ?>" class="btn btn-success dropdown-item" download><i class="fa fa-download"></i> Download</a>
	      		<a href="<?php echo $path.$row['file_path'] ?>" class="btn btn-info dropdown-item" target="_blank"><i class="fa fas fa-eye"></i> Open</a>

	      		<a href="#"  onclick="delete_js('<?php echo $row['file_name'] ?>','<?php echo $row['file_id'] ?>','<?php echo $row['file_path'] ?>');" class="btn btn-info dropdown-item"><i class="fa fas fa-trash"></i> Delete</a>
	      	</div>
	      	
	      </td>
	    </tr>
		<?php
	}
	break;

	case 'show_request':
		$data = array('id' => $id);
		$sql ="SELECT * from tbl_request where profile_id = :id and date_deleted is NULL";

		$prep = $con->prepare($sql);
		$prep->execute($data);
		$i = 0;
		while($row = $prep->fetch()){
		$i++;
		?>
		<tr>
	      <td class="text-truncate"><?php echo $i ?></td>
	      <td class="text-truncate"><?php echo $row['request_type'] ?></td>
	      <td class="text-truncate"><?php echo $row['date_registered'] ?></td>
	      <td class="text-truncate"><?php echo $row['date_request'] ?></td>
	      <td class="text-truncate"><?php echo request_status($row['approved']) ?></td>
	      <td class="text-center">
	      	<button class="btn btn-dark dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
	      	<div class="dropdown-menu">
	      		<a href="#" onclick="delete_record('<?php echo $row['request_id'] ?>','<?php echo $row['approved'] ?>');" class="btn btn-info dropdown-item"><i class="fa fas fa-trash"></i> Delete</a>
	      	</div>
	      </td>
	    </tr>
		<?php
	}
	break;

	case 'show_all_logs':
		$data = array('id' => $id);
		$sql ="SELECT a.*,b.* from tbl_log a left join tbl_profile b on a.profile_id=b.profile_id";

		$prep = $con->prepare($sql);
		$prep->execute($data);
		$i = 0;
		while($row = $prep->fetch()){
		$i++;
		?>
		<tr>
		  <td><?php echo $i ?></td>
	      <td class="text-truncate"><?php echo $row['lname'].', '.$row['fname'].' '.$row['mname'][0] ?></td>
	      <td class="text-truncate"><?php echo $row['message'] ?></td>
	      <td class="text-truncate"><?php echo $row['date_register'] ?></td>
	     <!--  <td class="text-center">
	      	<button class="btn btn-dark dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
	      	<div class="dropdown-menu">
	      		<a href="#" onclick="delete_record('<?php echo $row['request_id'] ?>','<?php echo $row['approved'] ?>');" class="btn btn-info dropdown-item"><i class="fa fas fa-trash"></i> Delete</a>
	      	</div>
	      </td> -->
	    </tr>
		<?php
	}
	break;

	case 'delete_file':
		$fileid = $_POST['id'];
		$file_path = $_POST['path'];

		$path = 'webroot/upload/'.$id.'/';

		$data = array('id' => $fileid);
		$sql = "DELETE from tbl_requirements where file_id=:id";
		if (delete_query($con,$data,$sql) > 0) {
			unlink($path.$file_path);
			echo 1;
		}
	break;

	case 'delete_request':
		$request_id = $_POST['id'];
		$status = $_POST['status'];
		$sql="";
		$data = array();

		$dt = array('req_id' => $request_id);
		$select = "SELECT a.*,b.* from tbl_request a left join tbl_profile b on a.profile_id=b.profile_id where a.request_id=:req_id";
		$rr = fetch_record($con,$dt,$select);

		$rows = $rr->fetch();

		$name = $rows['fname'].' '.$rows['lname'];


		if ($status == 1 || $status == 4 || $status == 2)  {
			$data = array('id' => $request_id, 'deleted' => date('Y-m-d'));
			$sql = "UPDATE tbl_request set date_deleted=:deleted where request_id=:id";
		}else{
			$data = array('id' => $request_id);
			$sql = "DELETE from tbl_request where request_id=:id";
		}

		if (delete_query($con,$data,$sql) > 0) {
			echo 1;
			logs($con,$name_log.' has delete the request of '.$name,$id_profile);

		}
	break;

	case 'save_request':
		$due_date = $_POST['due_date'];
		$type_request = $_POST['type_request'];
		$id_number = $_POST['id_number'];
		
		$data_verify = array('id' => $id);

		if (strtotime($due_date) < strtotime(date('Y-m-d'))) {
			echo 123;
		}else if (date('l',strtotime($due_date)) == 'Saturday' || date('l',strtotime($due_date)) == 'Sunday') {
			echo 301;
		}else{
			if (strlen($id_number) <= 15) {
			// Id format is not valid
					echo 404;
				}else{
					
					$sql_verify = "SELECT * from tbl_request where profile_id=:id and approved = 0 or approved = 1";
					$verify = verify_record($con,$data_verify,$sql_verify);

					if ($verify > 0) {
						// Have request already!
						echo 101;
					}else{


					$sql_check_requirements = "SELECT * from tbl_requirements where profile_id=:id and is_photo = 1";

					if (verify_record($con,$data_verify,$sql_check_requirements) > 0) {
						// Check if theres a 2x2 picture
						$data = array('profile_id' => $id, 'due_date' => $due_date, 'type_request' => $type_request, 'date_reg' => date('Y-m-d'));
						$sql = "INSERT INTO tbl_request(profile_id,date_request,request_type,date_registered) VALUES(:profile_id,:due_date,:type_request,:date_reg)";

							$result = save($con,$data,$sql);

							if ($result == 1) {
									$id_data = array('profid' => $id, 'new_id' => $id_number);
									$new_id_sql = "UPDATE tbl_profile set generated_id=:new_id where profile_id=:profid";

									logs($con,$name_log.' has request for ID.',$id_profile);

									echo save($con,$id_data,$new_id_sql);
							}
					}else{
						echo 202;
					}

					
					}
				}
		}

		
	break;
	// User

	// Profile
	case 'change_profile':
		$prof_id = (!isset($_POST['id'])) ? $id : $_POST['id'];
		$fn = $_POST['fn'];
		$mn = $_POST['mn'];
		$ln = $_POST['ln'];
		$gender = $_POST['gender'];
		$disability = (isset($_POST['disability']))? $_POST['disability'] : '';
		$address = $_POST['address'];
		$gname = (isset($_POST['gname']))? $_POST['gname'] : '';
		$contacts =(isset( $_POST['contacts']))?  $_POST['contacts'] : '';
		$birthdate = $_POST['bdate'];
		$blood_type = (isset($_POST['blood_type']))? $_POST['blood_type'] : '';


		$data = array('id' => $prof_id,'fn' => $fn, 'mn' => $mn, 'ln' => $ln,'gender' => $gender,'blood_type' => $blood_type,'disability' => $disability,'address' => $address,'gname' => $gname,'contacts' => $contacts, 'birthdate' => $birthdate);


		// echo print_r($data);
		$sql = "UPDATE tbl_profile set fname=:fn, mname=:mn, lname=:ln, gender=:gender,blood_type=:blood_type, disability_type=:disability,address=:address,guardian_name=:gname, guardian_number=:contacts, birthdate=:birthdate WHERE profile_id =:id";

		logs($con,$name_log.' has update the profile.',$id_profile);

		echo save($con,$data,$sql);


	break;

	case 'profile_picture':
		$profile_id = $auth['profile_id'];

		$data = array('prof' => $profile_id);
		
		$sql = "SELECT * from tbl_requirements where profile_id=:prof and is_photo = 1";
		$gender = $auth['gender'];
		$path = '';

		if (verify_record($con,$data,$sql) > 0) {
			
			$row = fetch_record($con,$data,$sql)->fetch();

			$path = '../webroot/upload/'.$profile_id.'/'.$row['file_path'];
		}else{
			$path = '../webroot/img/site/'.$gender.'.png';
		}

		echo $path;

	break;

	// session
	case 'get_sessions':
		echo json_encode(get_session($con,$id));
	break;

	// Email
	case 'change_email':
		$email = $_POST['email'];

		$data = array('id' => $id, 'email' => $email);

		$sql_verify = "SELECT * from tbl_account where email_address = :email and profile_id != :id";

		$result = verify_record($con,$data,$sql_verify);

		if ($result > 0) {
			echo 2;
		}else{
			$sql = "UPDATE tbl_account set email_address=:email where profile_id = :id";
			logs($con,$name_log.' has changed email address.',$id_profile);
			echo save($con,$data,$sql);
		}

	break;

	// Password
	case 'change_password':
		$o_password = $_POST['o_password'];
		$password = $_POST['password'];

		if (password_verify($o_password, $auth['password']) == true) {

			$hash_pwd = password_hash($password, PASSWORD_BCRYPT);


			$new_hash = str_replace("$2y$", "$2a$", $hash_pwd);

			$data = array('id' => $id, 'password' => $new_hash);
			$sql = "UPDATE tbl_account set password=:password where profile_id=:id";
			echo save($con,$data,$sql);
			logs($con,$name_log.' has changed password.',$id_profile);
		}else{
			echo 404;
		}
	break;

	// Admin
	case 'show_pending':

		$gender_filter = $_POST['gender_filter'];
		$address_filter = $_POST['address_filter'];
		$filter_name = $_POST['filter_name'];
		$disab_filter = $_POST['disab_filter'];
		
		$where_disab = '';

		$where_address = '';
		$where_gender = '';

		$where_filter = '';

		if (!empty($gender_filter)) {
			$where_gender = "and b.gender like '%$gender_filter%'";
		}

		if (!empty($address_filter)) {
			$where_address = "and b.address like '%$address_filter%'";
		}

		if (!empty($filter_name)) {
			$where_filter = "and (b.generated_id like '%$filter_name%' or b.fname like '%$filter_name%' or b.mname like '%$filter_name%' or b.lname like '%$filter_name%' or concat(b.fname,' ',b.lname) like '%$filter_name%')";
		}

		if (!empty($disab_filter)) {
			$where_disab = "and b.disability_type like '%$disab_filter%'";
		}

		$data = array();
		

		$types = $_POST['type'];

		$data = array('type' => intval($types));



		$sql = "SELECT a.*,b.*,c.* from tbl_request a left join tbl_profile b on a.profile_id=b.profile_id left join tbl_account c on a.profile_id=c.profile_id where a.approved = :type and a.date_deleted is NULL and c.user_type=3 ".$where_gender." ".$where_address."  ".$where_filter." ".$where_disab." ";


		$result = fetch_record($con,$data,$sql);
		$i = 0;
		while ($row = $result->fetch()) {
			$i++;		
			$name = ucfirst($row['lname']).', '.ucfirst($row['fname']).' '.$row['mname'];
		?>
		<tr>
		  <td><?php echo $i ?></td>
		  <td class="text-truncate"><?php echo $row['generated_id']  ?></td>
		  <td class="text-truncate"><?php echo $name  ?></td>
	      <td class="text-truncate"><?php echo $row['disability_type'] ?></td>
	      <td class="text-truncate"><?php echo $row['request_type'] ?></td>
	      <td class="text-truncate"><?php echo $row['date_registered'] ?></td>
	      <td class="text-truncate"><?php echo $row['date_request'] ?></td>
	      <td class="text-center">
	      	<button class="btn btn-dark dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
	      	<div class="dropdown-menu">
	      	
	      	<!-- 0 = pending,  1 = approve,  2 = claimed, 3 = disapprove, 4 = printed -->

	      	<?php if ($types == 3 || $types == 0): ?>
	      		<a href="#" onclick="approve_disapprove('<?php echo $name ?>','<?php echo $row['request_id'] ?>','approve', '<?php echo $types ?>','<?php echo $row['contact_number'] ?>');" class="btn btn-info dropdown-item"><i class="fa fas fa-thumbs-up text-info"></i> Approve</a>
	      	<?php endif ?>
	      	
      		<?php if ($types == 1): ?>
      		<!-- <a href="#" onclick="claim('<?php echo $name ?>','<?php echo $row['request_id'] ?>','claim', '<?php echo $types ?>');" class="btn btn-info dropdown-item"><i class="fa fas fa-check-circle text-success"></i> Claim</a> -->

      		<!-- <div class="dropdown-divider"></div> -->
	      	<?php endif ?>


	      	<?php if ($types == 4): ?>
      		<a href="#" onclick="claim('<?php echo $name ?>','<?php echo $row['request_id'] ?>','printed', '<?php echo $types ?>');" class="btn btn-info dropdown-item"><i class="fa fas fa-check-circle text-success"></i> Claim</a>
	      	<?php endif ?>


	      	<?php if ($types == 3 || $types == 1): ?>
	      		<a href="#" onclick="approve_disapprove('<?php echo $name ?>','<?php echo $row['request_id'] ?>','pending', '<?php echo $types ?>','<?php echo $row['contact_number'] ?>');" class="btn btn-info dropdown-item"><i class="fa fas fa-bell text-warning"></i> Mark as Pending</a>
	      	<?php endif ?>

	      	<?php if ($types == 0): ?>
	      		<a href="#" style="display: <?php echo $able ?>" onclick="approve_disapprove('<?php echo $name ?>','<?php echo $row['request_id'] ?>','disapprove', '<?php echo $types ?>','<?php echo $row['contact_number'] ?>');" class="btn btn-info dropdown-item"><i class="fa fas fa-thumbs-down text-danger"></i> Disapprove</a>
	      	<?php endif ?>
	      		
	      		  <div class="dropdown-divider"></div>
	      		<a href="#" onclick="show_requirements_details('<?php echo $row['profile_id'] ?>', '<?php echo $name ?>');" class="btn btn-info dropdown-item"><i class="fa fas fa-file text-primary"></i> Requirements</a>
	      	</div>
	      </td>
	    </tr>
		<?php
		}
	break;

	case 'approve_disapprove_request':
		$request_id = $_POST['id'];
		$setval = $_POST['set_val'];
		$r_name = $_POST['r_name'];
		$cp_number = $_POST['number'];
		$status = '';
		$data = array('id' => $request_id, 'value' => $setval);
		$sql = "UPDATE tbl_request set approved=:value where request_id = :id";
		
		if ($setval == 1) {
			$status = 'Approved';
		}else if ($setval == 3) {
			$status = 'Disapprove';
		}else if ($setval == 0) {
			$status = 'Pending';
		}else{
			$status = 'Mark as ready';
		}

	 	 $message = '';
		 $message .= "PWD's Online Reservation\n";
		 $message .= 'Hello, '.ucfirst($r_name).'!';
		 $message .= "\nYour request is ".$status."\n\n";

		 // if (send_sms_now($cp_number,$message) > 0) {
		 // }
			logs($con,$name_log.' has marked '.$r_name.' request as '.$status,$id_profile);

		echo save($con,$data,$sql);
	break;


	case 'claim_request':
		$request_id = $_POST['id'];
		$claimant = $_POST['claimant'];
		$setval = $_POST['set_val'];
		$r_name = $_POST['r_name'];
		$data = array('id' => $request_id, 'value' => $setval, 'claimant' => $claimant, 'date_claimed' => date('Y-m-d H:i:s A'));
		$sql = "UPDATE tbl_request set approved=:value,claimant_name=:claimant,date_claimed=:date_claimed where request_id = :id";
		logs($con,$name_log.' accepts '.$claimant.'\'s request to claim '.$r_name.'\'s ID.',$id_profile);

		echo save($con,$data,$sql);
	break;

	case 'notifications':
        $data = array();
        $sql = "SELECT a.*,b.*,c.* from tbl_profile a left join tbl_request b on a.profile_id = b.profile_id left join tbl_account c on b.profile_id=c.profile_id where approved = 0 and c.date_deleted is null and c.user_type = 3 LIMIT 4 ";

        $result = fetch_record($con,$data,$sql);
        
        while ($row = $result->fetch()) {
        ?>
           <a class="dropdown-item d-flex align-items-center" href="request.php">
            <div class="dropdown-list-image mr-3">
               <div class="icon-circle bg-primary">
                <i class="fas fa-bell text-white"></i>
              </div>
            </div>

            <div class="font-weight-bold">
              <div class="text-truncate"><?php echo $row['request_type'] ?> ID request.</div>
              <div class="small text-gray-500">From: <?php echo ucfirst($row['fname']).' '.ucfirst($row['lname']) ?></div>
            </div>
          </a>
        <?php
        }
	break;
	
	case 'requirements':
		$prof_id = $_POST['id'];
		$data = array('id' => $prof_id);
		$sql ="SELECT * from tbl_requirements where profile_id = :id";

		$prep = $con->prepare($sql);
		$prep->execute($data);

		$path = '../webroot/upload/'.$prof_id.'/';

		while($row = $prep->fetch()){
		?>
		<tr>
	      <td class="text-truncate"><?php echo $row['file_name'] ?></td>
	      <td class="text-truncate" ><?php echo $row['date_registered'] ?></td>
	      <td class="text-center">
	      	<button class="dropdown-toggle btn btn-dark" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
	      	<div class="dropdown-menu">
      			<a href="<?php echo $path.$row['file_path'] ?>" class="btn btn-info dropdown-item" target="_blank"><i class="fa fas fa-eye"></i> View </a>
	     	 	<a href="<?php echo $path.$row['file_path'] ?>" class="btn btn-success dropdown-item" download><i class="fa fa-download"></i> Download</a>
	      	</div>
	      
	      </td>

	    </tr>
		<?php
	}
	break;

	case 'alert_count':
		echo count_users($con,'pending');
	break;


	case 'showStaff':
		
		$data = array();
		$sql ="SELECT a.*,b.* from tbl_profile a left join tbl_account b on a.profile_id=b.profile_id where b.is_approve = 1 and b.user_type=2 and b.date_deleted is null";

		$prep = $con->prepare($sql);
		$prep->execute($data);
		$i = 0;
		while($row = $prep->fetch()){
			$i++;
			$name = ucfirst($row['lname']).', '.ucfirst($row['fname']).' '.$row['mname'];
		?>
		<tr>
			<td><?php echo $i ?></td>
	      <td class="text-truncate"><?php echo $name ?></td>
	      <td class="text-truncate"><?php echo $row['address'].', Santiago City' ?></td>
	      <td class="text-truncate"><?php echo $row['email_address'] ?></td>
	      <td class="text-truncate" ><?php echo $row['date_registered'] ?></td>
	      <td class="text-center">
	      		<button class="btn btn-dark dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
		      	<div class="dropdown-menu">
		      		<a href="#" onclick="delete_account('<?php echo $row['profile_id'] ?>','<?php echo $name ?>');" class="btn btn-info dropdown-item"><i class="fa fas fa-trash"></i> Delete</a>

		      		<a href="#" onclick="edit_clear_admin('<?php echo $row['profile_id'] ?>','<?php echo $row['fname'] ?>','<?php echo $row['mname'] ?>','<?php echo $row['lname'] ?>','<?php echo $row['gender'] ?>','<?php echo $row['disability_type'] ?>','<?php echo $row['address'] ?>','<?php echo $row['email_address'] ?>','<?php echo $row['guardian_name'] ?>','<?php echo $row['guardian_number'] ?>','<?php echo $row['contact_number'] ?>','<?php echo $row['birthdate'] ?>','<?php echo $row['blood_type'] ?>'); $('#registerModalStaff').modal('show');" class="btn btn-info dropdown-item"><i class="fa fas fa-edit"></i> Edit</a>
		      	</div>
	      </td>

	    </tr>
		<?php
	}
	break;

	case 'showPwd':
		$gender_filter = $_POST['gender_filter'];
		$address_filter = $_POST['address_filter'];
		$filter_name = $_POST['filter_name'];
		$disab_filter = $_POST['disab_filter'];
		
		$where_disab = '';

		$where_address = '';
		$where_gender = '';

		$where_filter = '';

		if (!empty($gender_filter)) {
			$where_gender = "and a.gender like '%$gender_filter%'";
		}

		if (!empty($address_filter)) {
			$where_address = "and a.address like '%$address_filter%'";
		}

		if (!empty($filter_name)) {
			$where_filter = "and (a.generated_id like '%$filter_name%' or a.fname like '%$filter_name%' or a.mname like '%$filter_name%' or a.lname like '%$filter_name%' or concat(a.fname,' ',a.lname) like '%$filter_name%')";
		}

		if (!empty($disab_filter)) {
			$where_disab = "and a.disability_type like '%$disab_filter%'";
		}

		$data = array();
		$sql ="SELECT a.*,b.* from tbl_profile a left join tbl_account b on a.profile_id=b.profile_id where b.is_approve = 1 and b.user_type= 3 and b.date_deleted is null ".$where_gender." ".$where_address."  ".$where_filter." ".$where_disab." ";

		$prep = $con->prepare($sql);
		$prep->execute($data);
		$i = 0;
			while($row = $prep->fetch()){
					$i++;
					$name = ucfirst($row['lname']).', '.ucfirst($row['fname']).' '.$row['mname'];
				?>
				<tr>
				  <td><?php echo $i ?></td>
			      <td class="text-truncate"><?php echo $row['generated_id'] ?></td>
			      <td class=""><?php echo $name ?></td>
			      <td class=""><?php echo $row['gender'] ?></td>
			      <td class=""><?php echo $row['address'].', Santiago City' ?></td>
			      <td class=""><?php echo $row['contact_number'] ?></td>
			      <td class=""><?php echo $row['disability_type'] ?></td>
			      <td class="" ><?php echo $row['date_registered'] ?></td>
			      <td class="text-center">
			      		<button class="btn btn-dark dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
				      	<div class="dropdown-menu">
				      		<a href="#" onclick="delete_account('<?php echo $row['profile_id'] ?>','<?php echo $name ?>');" class="btn btn-info dropdown-item"><i class="fa fas fa-trash"></i> Delete</a>

				      		<a href="#" onclick="edit_clear_admin('<?php echo $row['profile_id'] ?>','<?php echo $row['fname'] ?>','<?php echo $row['mname'] ?>','<?php echo $row['lname'] ?>','<?php echo $row['gender'] ?>','<?php echo $row['disability_type'] ?>','<?php echo $row['address'] ?>','<?php echo $row['email_address'] ?>','<?php echo $row['guardian_name'] ?>','<?php echo $row['guardian_number'] ?>','<?php echo $row['contact_number'] ?>','<?php echo $row['birthdate'] ?>','<?php echo $row['blood_type'] ?>'); $('#registerModal').modal('show');" class="btn btn-info dropdown-item"><i class="fa fas fa-edit"></i> Edit</a>
				      	</div>
			      </td>

			    </tr>
				<?php
			}
	 break;


	case 'register_walkin':
		$prof_id = $_POST['profile_id'];
		$fn = $_POST['fn'];
		$mn = $_POST['mn'];
		$ln = $_POST['ln'];
		$gender = $_POST['gender'];
		$disability = (isset($_POST['disability']))? $_POST['disability'] : '';
		$address = $_POST['address'];
		$email = $_POST['email'];
		$cp_number = $_POST['cp_number'];
		$gname = (isset($_POST['gname']))? $_POST['gname'] : '';
		$contacts =(isset( $_POST['contacts']))?  $_POST['contacts'] : '';
		$birthdate = $_POST['bdate'];
		$status = $_POST['status'];
		
		$blood_type = $_POST['blood_type'];

		$data_verify = array('emails' => $email, 'profid' => $prof_id);

		$verify = "SELECT * from tbl_account Where email_address = :emails and profile_id != :profid";
	    
		$prep = $con->prepare($verify);
		$prep->execute($data_verify);

		 if ($prep->rowCount() > 0) {
			echo 2;
		 }else{

		 	// Generate ID number
		 	$max_id = get_last_id($con);
		 	$generate_id = generate_id($max_id);
		 	// Generate ID number

		 	if (!empty($prof_id)) {
		 		$datas = array('prof_id' => $prof_id,'fn' => $fn, 'mn' => $mn, 'ln' => $ln,'gender' => $gender,'contact_number' => $cp_number,'blood_type' => $blood_type,'disability' => $disability,'address' => $address,'gname' => $gname,'contacts' => $contacts, 'birthdate' => $birthdate);

		 		$sqls = "UPDATE tbl_profile set fname=:fn, mname=:mn, lname=:ln, gender=:gender, contact_number=:contact_number,blood_type=:blood_type, disability_type=:disability,address=:address,guardian_name=:gname, guardian_number=:contacts, birthdate=:birthdate WHERE profile_id=:prof_id";

		 		if (save($con,$datas,$sqls) > 0) {
		 			$data_email = array('emails' => $email, 'profile_id' => $prof_id);
		 			$email_save = "UPDATE tbl_account set email_address=:emails where profile_id=:profile_id";
		 			echo save($con,$data_email,$email_save);
					logs($con,$name_log.' has modified '.$fn.' '.$ln.' account.',$id_profile);

		 			// echo "string";
		 		}
		 	}
		 	else
		 	{
		 		 $data = array('generated_id' => $generate_id,'fn' => $fn, 'mn' => $mn, 'ln' => $ln,'gender' => $gender,'contact_number' => $cp_number,'blood_type' => $blood_type,'disability' => $disability,'address' => $address,'gname' => $gname,'contacts' => $contacts,'date_reg' => date('Y-m-d'), 'birthdate' => $birthdate);
				 $sql = "INSERT INTO tbl_profile (generated_id,fname,mname,lname,birthdate,gender,contact_number,blood_type,address,disability_type,guardian_name,guardian_number,date_registered) VALUES(:generated_id,:fn,:mn,:ln,:birthdate,:gender,:contact_number,:blood_type,:address,:disability,:gname,:contacts,:date_reg)";

				 
				 // $password = $ln;

				 // get last id from profile.
				 $profile_id = GetLastId($con,$data,$sql);
				 // Create access token for verification
				 $token = $fn.$ln.$profile_id;



				 $seed = str_split('abcdefghijklmnopqrstuvwxyz'.'ABCDEFGHIJKLMNOPQRSTUVWXYZ'); // and any other characters
		     	 shuffle($seed); // probably optional since array_is randomized; this may be redundant
		    	 $rand = '';
		     	 foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];

		     	 $password = $rand.$profile_id;

				 
				 $access_token = password_hash($token, PASSWORD_DEFAULT);

				 $password_hashed = password_hash($password, PASSWORD_BCRYPT);

				 $new_hash = str_replace("$2y$", "$2a$", $password_hashed);
				 
				 $result = register_account($con,$profile_id,$email,$new_hash,$access_token,1,$status);

				 // Make verification link
				 // $link = '192.168.1.2:82/PWD_online_reservation/verify.php?API_token='.$access_token.'&id='.$profile_id;

				 $message = '';
				 $message .= "PWD's Online Reservation\n";
				 $message .= 'Hello, '.ucfirst($fn).' '.ucfirst($ln).'!';
				 $message .= "\nYour Password is (".$password.")\n\n";

				 if (send_sms_now($cp_number,$message) > 0) {
				logs($con,$name_log.' has added '.$fn.' '.$ln.' account.',$id_profile);

				 	echo 1;
				 }
		 	}


		 	
		 }
	break;

	case 'dash_count':
		$types = $_POST['types'];
		echo count_users($con,$types);
	break;

	case 'claimed_id':
		$data = array();
		$sql ="SELECT a.*,b.*,c.* from tbl_profile a left join tbl_account b on a.profile_id=b.profile_id left join tbl_request c on a.profile_id=c.profile_id where c.approved = 2 and b.user_type=3 and c.date_deleted is Null";

		$prep = $con->prepare($sql);
		$prep->execute($data);
		$i = 0;
		while($row = $prep->fetch()){
			$i++;
			$name = ucfirst($row['lname']).', '.ucfirst($row['fname']).' '.$row['mname'];
		?>
		<tr>
			<td><?php echo $i ?></td>
          <td class="text-truncate"><?php echo $row['generated_id'] ?></td>
	      <td class="text-truncate"><?php echo $name ?></td>
          <td class="text-truncate"><?php echo $row['address'] ?></td>
	      <td class="text-truncate"><?php echo $row['request_type'] ?></td>
	      <td class="text-truncate"><?php echo $row['claimant_name'] ?></td>
	      <td class="text-truncate"><?php echo date('Y/m/d',strtotime($row['date_claimed'])); ?></td>
	     <!--  <td class="text-center">
	      		<button class="btn btn-dark dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></button>
		      	<div class="dropdown-menu">
		      		<a href="#" onclick="delete_claimed('<?php echo $row['request_id'] ?>');" class="btn btn-info dropdown-item"><i class="fa fas fa-trash"></i> Delete</a>
		      	</div>
	      </td> -->

	    </tr>
		<?php
	}
	break;

	case 'delete_claimed':
		$request_id = $_POST['id'];	

		$data = array('id' => $request_id, 'deleted' => date('Y-m-d'));
		$sql = "UPDATE tbl_request set date_deleted=:deleted where request_id=:id";

		if (save($con,$data,$sql) > 0) {
			echo 1;

		}
	break;

	case 'delete_account':
		$profid = $_POST['id'];
		$r_name = $_POST['r_name'];
		$data = array('prof_id' => $profid, 'is_delete' => date('Y-m-d'));
		$sql = "UPDATE tbl_account set date_deleted = :is_delete where profile_id=:prof_id";

		echo save($con,$data,$sql);
		logs($con,$name_log.' has delete '.$r_name.'\'s account.',$id_profile);

	break;

	default:
		echo "undefined action name ".$action;
	break;
}


?>