<?php session_start();
if (isset($_SESSION['user_type']) == 3) {
  @header('location:user/');
}else if (isset($_SESSION['user_type']) == 2) {
  @header('location:staff/');
}else if (isset($_SESSION['user_type']) == 1) {
  @header('location:admin');
}
include("config.php");
include("function.php");
 ?>
<?php  ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>PWD Online Appointment</title>


  <link rel="icon" type="icon/png" href="webroot/img/site/logo2.png">
  <!-- Custom fonts for this theme -->
  <link href="webroot/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="webroot/assets/css/animate.css">
  <link rel="stylesheet" type="text/css" href="webroot/assets/css/sweetalert.css">
  <!-- Theme CSS -->
  <link href="webroot/css/freelancer.css" rel="stylesheet">
</head> 

 <style type="text/css">
    .mt {
      margin-top: 60px;
    }
    @media only screen and (max-width: 600px) {
     .mt {
      margin-top: 90px;
    }
    .header-text{
      display: none;
    }
    }
</style>


<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top navbar-shrink"  id="mainNav">
    <div class="container-fluid p-0 m-0">
      <a class="navbar-brand js-scroll-trigger" href="#page-top" id="text-head"><i class="fa fa-wheelchair fa-2x bg-primary p-2 rounded" style="background-color: #165296 !important; border: solid 1px #fff;"></i> <span class="header-text">PWD's ID ONLINE APPOINTMENT RESERVATION</span></a>
      <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto" style="font-size: 0.9em;">
          <li class="nav-item">
            <a class="nav-link py-3 px-0 px-lg-3  js-scroll-trigger rounded" href="#portfolio"><i class="fa fa-question-circle fa-2x"></i> How it works ?</a>
          </li>
          <li class="nav-item ">
            <a class="nav-link py-3 px-0 px-lg-3  js-scroll-trigger rounded" href="#about"><i class="fa fa-info-circle fa-2x"></i> Qualifications</a>
          </li>
          <li class="nav-item ">
            <a class="nav-link py-3 px-0 px-lg-3 js-scroll-trigger rounded" href="#contact"><i class="fa fa-user-edit fa-2x"></i> Register</a>
          </li>
          <li class="nav-item ">
            <a class="nav-link py-3 px-0 px-lg-3" href="#login" data-toggle="modal" data-backdrop="static" data-target="#login"><i class="fa fa-user-lock fa-2x"></i> Login</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>


  
   <img class="img-fluid mt animated fadeIn" src="webroot/img/site/BG.png" alt="santigo City">
  <!-- Portfolio Section -->
  <section class="page-section portfolio " id="portfolio" style="background-color: #ccfff8">
    <div class="container">

      <!-- Portfolio Section Heading -->
      <h3 class="text-center text-uppercase text-secondary mb-0">How it works ?</h3>

      <!-- Icon Divider -->
      <div class="divider-custom">
        <div class="divider-custom-line"></div>
        <div class="divider-custom-icon">
          <i class="fas fa-question-circle"></i>
        </div>
        <div class="divider-custom-line"></div>
      </div>

      <!-- Portfolio Grid Items -->
      <div class="row">

        <!-- Portfolio Item 1 -->
        <div class="col-md-3 col-lg-3">
          <div class="portfolio-item mx-auto" >
            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
              <div class="portfolio-item-caption-content text-center text-white">
                Register
              </div>
            </div>
            <img class="img-fluid" src="webroot/img/site/register.png" alt="">
          </div>
           <p class="lead text-dark text-center">Sign up and verify your account using your mobile number and Login to your account.</p>
        </div>

        <!-- Portfolio Item 2 -->
        <div class="col-md-3 col-lg-3">
          <div class="portfolio-item mx-auto" >
            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
              <div class="portfolio-item-caption-content text-center text-white">
                Upload your Valid ID
              </div>
            </div>
            <img class="img-fluid" src="webroot/img/site/provide.png" alt="">
          </div>
          <p class="lead text-dark text-center">Upload supporting documents in your account.</p>
        </div>


        <div class="col-md-3 col-lg-3">
          <div class="portfolio-item mx-auto" >
            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
              <div class="portfolio-item-caption-content text-center text-white">
                Set Appointment Date.
              </div>
            </div>
            <img class="img-fluid" src="webroot/img/site/set.png" alt="">
          </div>
          <p class="lead text-dark text-center">Request your appointment.</p>
        </div>

        <!-- Portfolio Item 3 -->
        <div class="col-md-3 col-lg-3">
          <div class="portfolio-item mx-auto" >
            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
              <div class="portfolio-item-caption-content text-center text-white">
                Claim your ID
              </div>
            </div>
            <img class="img-fluid" src="webroot/img/site/claim.png" alt="">
          </div>
          <p class="lead text-dark text-center">Claim your ID on your Appointment Date.</p>
        </div>

         

      </div>
      <!-- /.row -->

    </div>
  </section>

  <!-- About Section -->
  <section class="page-section  text-dark mb-0" id="about">
    <div class="container">

      <!-- About Section Heading -->
      <h3 class=" text-center text-uppercase text-dark"> Who qualifies for the PWD ID?</h3>

      <!-- Icon Divider -->
      <div class="divider-custom divider-light">
        <div class="divider-custom-line"></div>
        <div class="divider-custom-icon">
          <i class="fas fa-info-circle"></i>
        </div>
        <div class="divider-custom-line"></div>
      </div>

      <!-- About Section Content -->
      <div class="row">
        <div class="col-lg-12 ml-auto">
          <p class="lead">
            Many of those who have disabilities have yet to claim their PWD ID and this article is for those who do not know if they qualify for the PWD ID and be able to claim the benefits and privileges that go with it.
            <br><br>
            According to Dr. Myla Rostrata of the Department of Health in Region 3, there are seven categories that classify the ID applicants These categories provide the basis for the granting of the IDs. These categories are:
         </p>
        </div>


        <div class="col-lg-12 mr-auto">
          <p class="lead">
            <span class="h4">Visual impairment</span> – as far as we in PWDPhil know, there is no set grade that is considered to be the threshold that separates the disabled from the non-disabled. However, DOH specialists in the PDAO (persons with disabilities affairs office) in your town or city hall have a set protocol for determining who is visually impaired and who can qualify for a PWD ID based on visual acuity, or the lack of it. If the applicant’s eyes are discernible to be blind, then the applicant need not bring a clinical abstract to prove the disability, just bring your government-issued ID.
          </p>
        </div>


        <div class="col-lg-12 mr-auto">
          <p class="lead">
            <span class="h4">Hearing loss</span> – anyone who cannot hear is considered to be disabled. However, there are various degrees of hearing impairment depending on the cause of the hearing loss. If you think that you are beginning to lose your hearing, it is best to try to have it treated right away. There are many occupations the depend on one’s ability to hear. If the applicant’s inability to hear is apparent, then there is no need to bring a clinical abstract to the PDAO when applying for the PWD ID.
          </p>
        </div>

        <div class="col-lg-12 mr-auto">
          <p class="lead">
            <span class="h4">Orthopedic disability</span> – this category includes all those who have had amputations done to any of the extremities. This also includes those who have stunted growth because of dwarfism, a medical condition where the patient’s height and extremities did not grow to full mature size.
          </p>
        </div>

         <div class="col-lg-12 mr-auto">
          <p class="lead">
            <span class="h4">Learning disability</span> – for students who cannot adjust to mainstream regular classes, they will need to go through the special education class program found in every public school. A learning disability is something that hampers or interferes with a student’s ability to learn basic concepts in math and science, knowledge that will help the student cope with everyday life. Without a basic education, the student will be unable to buy groceries, pay bills, earn a living or do any activity that requires critical thinking and stock knowledge.
          </p>
        </div>

        <div class="col-lg-12 mr-auto">
          <p class="lead">
            <span class="h4">Psychosocial disability</span> – this is a category that includes those who have psychological and social problems that can leave them the sufferer unable to do what he/she normally does. These disabilities are not apparent and may not be easy to understand or to explain to health center workers who do not have a science background, thus the need for a clinical abstract to present to the PDAO, health center or DSWD regional office workers. Normally there are medical professionals who assess the application for PWD IDs, though some of them are not regularly available.
          </p>
        </div>

         <div class="col-lg-12 mr-auto">
          <p class="lead">
            <span class="h4">Chronic illness</span> – There are illnesses that may not inherently lead to a disability but have been known to cause disability when a complication sets in. One such case is diabetes. Inherently diabetes does not cause disability. However, the inability to process sugar in your body may lead to complications such as diabetic retinopathy which could lead to blindness, sepsis which could lead to gangrene and later, amputation, or heart problems which could lead to a stroke. Again, some chronic illnesses are not apparent and will need a clinical abstract before a PWD ID is granted.
          </p>
        </div>

        <div class="col-lg-12 mr-auto">
          <p class="lead">
            <span class="h4">Mental disability</span>– Sufferers of mental illness are automatically considered to be disabled and are sometimes automatically granted a PWD ID, as some mental illnesses can be apparent. Those with mental disabilities will usually need medical attention, constant supervision and assisted living arrangements as they are mostly unable to take care of themselves. Included in this category are children with Down syndrome, bi-polar disorder, post traumatic stress disorder and schizophrenia. Sufferers of this category will need a medical/clinical abstract prior to the issuance of a PWD ID
          </p>
        </div>

         <div class="col-lg-12 mr-auto">
          <p class="lead">
            Those who apply for PWD IDs must also remind their doctors or medical professionals that any information about the state or the nature of their disability must be handled with utmost confidentiality. A Pinoy with disability has the right not to divulge any medical or health-related information about themselves. As the non-disabled people have the right not be harassed for their medical informatio, PWDs should also be allowed to exercise the same right.
            <br><br>
            Source : <a href="http://pwdphil.com/2017/11/16/qualifies-pwd-id/" target="_blank">http://pwdphil.com</a>
          </p>
        </div>


      </div>

    </div>
  </section>

  <!-- Contact Section -->
  <section class="page-section " style="background-color: #ccfff8" id="contact">
    <div class="container">

     

      <!-- Contact Section Form -->
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
          <div class="card">
            <div class="card-header">
               <!-- Contact Section Heading -->
                <h3 class=" text-center text-uppercase text-secondary mb-0">Registration Form</h3>
            </div>
            <div class="card-body">
                <div class="row">
                  <div class="col-sm-6 form-group">
                    <label for="fn">Firstname</label>
                    <input type="text" name="fn" id="fn" class="form-control" placeholder="Enter Firstname...">
                    <span class="text-danger" id="fn_alert"></span>
                  </div>
                  
                  <div class="col-sm-6 form-group">
                    <label for="mn">Middlename</label>
                    <input type="text" name="mn" id="mn" class="form-control" placeholder="Enter Middlename...">
                    <span class="text-danger" id="mn_alert"></span>
                  </div>

                  <div class="col-sm-6 form-group">
                    <label for="ln">Lastname</label>
                    <input type="text" name="ln" id="ln" class="form-control" placeholder="Enter Lastname...">
                    <span class="text-danger" id="ln_alert"></span>
                  </div>
                  <div class="col-sm-6 form-group">
                    <label for="bdate">Birthdate</label>
                    <input type="date" name="bdate" id="bdate" class="form-control" placeholder="Enter Lastname...">
                    <span class="text-danger" id="date_alert"></span>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-6 form-group">
                    <label for="gender">Sex</label>
                    <select class="form-control" id="gender">
                      <option selected="" disabled="" value="">Select your gender</option>
                      <option>MALE</option>
                      <option>FEMALE</option>
                    </select>
                    <span class="text-danger" id="gender_alert"></span>
                  </div>

                  <div class="col-sm-6 form-group">
                    <label for="blood_type">Blood Type</label>
                    <select class="form-control" id="blood_type">
                      <option selected="" disabled="" value="">Select your blood type</option>
                      <option value="AB+">AB-positive</option>
                      <option value="AB-">AB-negative</option>
                      <option value="B+">B-positive</option>
                      <option value="B-">B-negative</option>
                      <option value="A+">A-positive</option>
                      <option value="A-">A-negative</option>
                      <option value="O+">O-positive</option>
                      <option value="O-">O-negative</option>
                    </select>
                    <span class="text-danger" id="blood_type_alert"></span>
                  </div>

                   <div class="col-sm-12 form-group">
                    <label for="disability">Disability</label>
                    <select name="disability" id="disability" class="form-control">
                      <option></option>
                      <option>Psychosocial Disability</option>
                      <option>Mental Disability</option>
                      <option>Hearing Disability</option>
                      <option>Visial Disability</option>
                      <option>Speech impairment</option>
                      <option>Learning Disability</option>
                      <option>Orthopedic (Musculoskeletal) Disability</option>
                    </select>
                    

                    <span class="text-danger" id="dis_alert"></span>
                  </div>

                  <div class="col-sm-12 form-group">
                        <label for="address">Address</label>
                    <select  name="address" id="address" class="form-control">
                      <option></option>
                      <?php echo select_address(); ?>
                    </select>
                    <span class="text-danger" id="address_alert"></span>
                  </div>


                  <div class="col-sm-6 form-group">
                    <label for="email">Email</label>
                    <input type="text" name="email" id="email" class="form-control" placeholder="Enter Email address..." autocomplete="off">
                    <span class="text-danger" id="email_alert"></span>
                  </div>
                   
                   <div class="col-sm-6 form-group">
                    <label for="email">Contact Number</label>
                    <input type="text" name="cp_number" id="cp_number" maxlength="11" onkeypress="return num_only(event);" class="form-control" placeholder="Enter contact number..." autocomplete="off">
                    <span class="text-danger" id="cp_number_alert"></span>
                  </div>

                  <div class="col-sm-12 form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" class="form-control" placeholder="Enter password..." >
                    <span class="text-danger" id="password_alert"></span>
                  </div>

                  <div class="col-sm-12 form-group">
                    <label for="cpassword">Confirm Password</label>
                    <input type="password" name="cpassword" id="cpassword" class="form-control" placeholder="Confirm password..." >
                    <span class="text-danger" id="cpassword_alert"></span>
                  </div>


                  <div class="col-sm-12">
                     <div class="divider-custom">
                      <div class="divider-custom-line"></div>
                      <div class="divider-custom-icon">
                       <small>IN CASE OF EMERGENCY</small>
                      </div>
                      <div class="divider-custom-line"></div>
                    </div>
                  </div>
                  <div class="col-sm-6 form-group">
                    <label for="gname">Name</label>
                    <input type="text" name="gname" id="gname" class="form-control" placeholder="Enter guardian fullname...">
                    <span class="text-danger" id="gname_alert"></span>
                  </div>

                  <div class="col-sm-6 form-group">
                    <label for="contacts">Contact number</label>
                    <input type="text" name="contacts" id="contacts" maxlength="11" onkeypress="return num_only(event);" class="form-control" placeholder="Enter guardian contact number...">
                    <span class="text-danger" id="contact_alert"></span>
                  </div>

                  <div class="form-group col-sm-12 text-right">
                    <br>
                    <button class="btn btn-primary btn-lg" id="btn_submit" onclick="register();"><i class="fa fa-save"></i> Register</button>
                  </div>

                </div>

            </div>
          </div>
        
        </div>
      </div>

    </div>
  </section>

  <!-- Footer -->
  <footer class="footer text-center">
    <div class="container">
      <div class="row">
        <!-- Footer About Text -->
        <div class="col-lg-12">
          <h4 class="text-uppercase mb-4">ABOUT PWD's ID ONLINE APPOINTMENT RESERVATION</h4>
          <p class="lead mb-0">
           <small> 
            The <a href="#page-top" class="js-scroll-trigger"><b class="text-primary">PWD's ID ONLINE APPOINTMENT RESERVATION </b></a> is the online registration <br> for those persons with Disabilities that
            cannot go personally to the PWD office <br> who needs to get their ID because of their situation.
           </small>
          </p>
        </div>
<!-- 
        PWD ID ONLINE APPOINTMENT RESERVATION is the online registration for those Person with Disability that
can't go for some reason personally to the PWD office who needs to get their ID -->

      </div>
    </div>
  </footer>

  <!-- Copyright Section -->
  <section class="copyright py-4 text-center text-white">
    <div class="container">
      <small>Copyright &copy; Online Appointment Reservation <br> with Record Management and ID System for PWDs of Santiago City 2019</small>
    </div>
  </section>

  <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
  <div class="scroll-to-top d-lg-none position-fixed ">
    <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
      <i class="fa fa-chevron-up"></i>
    </a>
  </div>

  <!-- Portfolio Modals -->
  <?php include('modal.php') ?>

  <!-- Bootstrap core JavaScript -->
  <script src="webroot/vendor/jquery/jquery.min.js"></script>
  <script src="webroot/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="webroot/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Contact Form JavaScript -->
  <script src="webroot/js/jqBootstrapValidation.js"></script>
  <script src="webroot/js/contact_me.js"></script>

  <!-- Custom scripts for this template -->
  <script src="webroot/js/freelancer.js"></script>
  <script src="webroot/js/index.js"></script>
  <script src="webroot/js/tools.js"></script>
  <script src="webroot/assets/js/sweetalert.min.js"></script>

</body>

</html>
