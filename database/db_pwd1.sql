/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1_3306
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : db_pwd

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2020-05-19 12:42:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_account
-- ----------------------------
DROP TABLE IF EXISTS `tbl_account`;
CREATE TABLE `tbl_account` (
  `account_id` int(255) NOT NULL AUTO_INCREMENT,
  `profile_id` int(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `password` longtext CHARACTER SET utf8,
  `access_token` longtext CHARACTER SET utf8,
  `user_type` int(1) DEFAULT '3',
  `date_registered` date DEFAULT NULL,
  `date_deleted` date DEFAULT NULL,
  `is_approve` int(1) DEFAULT '0',
  `is_forgot` int(1) DEFAULT '0',
  `verification_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`account_id`),
  KEY `profile` (`profile_id`),
  CONSTRAINT `profile` FOREIGN KEY (`profile_id`) REFERENCES `tbl_profile` (`profile_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_account
-- ----------------------------
INSERT INTO `tbl_account` VALUES ('12', '13', 'admin@gmail.com', '$2a$10$rW94Tzt7IynJAXeWAcmTV.EFM.JBIVaBoJzI2i6wlUm/4BmIt.kO2', null, '1', '2019-09-05', null, '1', '0', '');
INSERT INTO `tbl_account` VALUES ('13', '14', 'sub1@gmail.com', '$2a$10$rW94Tzt7IynJAXeWAcmTV.EFM.JBIVaBoJzI2i6wlUm/4BmIt.kO2', '$2y$10$ryQF6bUe4hjasLKgykDJ8.Y2xEO4g7y2zv92bWCSwwZinMYy95M46', '2', '2019-09-05', null, '1', '0', '');
INSERT INTO `tbl_account` VALUES ('14', '16', 'sub2@gmail.com', '$2a$10$rW94Tzt7IynJAXeWAcmTV.EFM.JBIVaBoJzI2i6wlUm/4BmIt.kO2', '$2y$10$hHK7rgp4HizjxQCs7DZ/b.mCE7uNBIv/P1yGKn.Vj5RzxisqHF9qq', '3', '2019-09-05', null, '1', '0', '');
INSERT INTO `tbl_account` VALUES ('16', '20', 'user@gmail.com', '$2a$10$rW94Tzt7IynJAXeWAcmTV.EFM.JBIVaBoJzI2i6wlUm/4BmIt.kO2', '$2y$10$iELXmrErpC4psTXmcRS2Nea3Qy0fMd.WmzqkPUe6jP29tNxyMLb6m', '3', '2020-03-09', null, '1', '0', 'BVmAK16');

-- ----------------------------
-- Table structure for tbl_authsig
-- ----------------------------
DROP TABLE IF EXISTS `tbl_authsig`;
CREATE TABLE `tbl_authsig` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `sign` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_authsig
-- ----------------------------

-- ----------------------------
-- Table structure for tbl_log
-- ----------------------------
DROP TABLE IF EXISTS `tbl_log`;
CREATE TABLE `tbl_log` (
  `log_id` int(255) NOT NULL AUTO_INCREMENT,
  `profile_id` int(255) DEFAULT NULL,
  `message` longtext,
  `date_register` datetime DEFAULT NULL,
  `date_deleted` date DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `profile_id` (`profile_id`),
  CONSTRAINT `profile_id` FOREIGN KEY (`profile_id`) REFERENCES `tbl_profile` (`profile_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_log
-- ----------------------------
INSERT INTO `tbl_log` VALUES ('17', '13', 'Juan, Dela Cruz has been Approved Guya, Jeffrey Luis A.\'s request.', '2020-03-27 00:00:00', null);
INSERT INTO `tbl_log` VALUES ('18', '13', 'Juan, Dela Cruz has been Disapprove Calter, Nicolas S.\'s request.', '2020-03-27 00:00:00', null);
INSERT INTO `tbl_log` VALUES ('19', '13', 'Juan, Dela Cruz has been Pending Guya, Jeffrey Luis A.\'s request.', '2020-03-27 00:00:00', null);
INSERT INTO `tbl_log` VALUES ('20', '13', 'Juan, Dela Cruz has been Approved Calter, Nicolas S.\'s request.', '2020-03-27 00:00:00', null);
INSERT INTO `tbl_log` VALUES ('21', '13', 'Juan, Dela Cruz has been Approved Guya, Jeffrey Luis A.\'s request.', '2020-03-27 00:00:00', null);
INSERT INTO `tbl_log` VALUES ('22', '13', 'Juan, Dela Cruz accepts Testing\'s request to claim Guya, Jeffrey Luis A.\'s ID.', '2020-03-27 00:00:00', null);
INSERT INTO `tbl_log` VALUES ('23', '13', 'Juan Dela Cruz has been login.', '2020-04-28 00:00:00', null);
INSERT INTO `tbl_log` VALUES ('24', '13', 'Juan Dela Cruz has been login.', '2020-05-01 00:00:00', null);
INSERT INTO `tbl_log` VALUES ('25', '13', 'Juan, Dela Cruz has  Pending Calter, Nicolas S.\'s request.', '2020-05-01 00:00:00', null);
INSERT INTO `tbl_log` VALUES ('26', '13', 'Juan, Dela Cruz has  Pending Calter, Nicolas S.\'s request.', '2020-05-01 00:00:00', null);
INSERT INTO `tbl_log` VALUES ('27', '13', 'Juan, Dela Cruz has  Pending Calter, Nicolas S.\'s request.', '2020-05-01 00:00:00', null);
INSERT INTO `tbl_log` VALUES ('28', '13', 'Juan, Dela Cruz has  Disapprove Calter, Nicolas S.\'s request.', '2020-05-01 00:00:00', null);
INSERT INTO `tbl_log` VALUES ('29', '13', 'Juan Dela Cruz has login.', '2020-05-07 00:00:00', null);
INSERT INTO `tbl_log` VALUES ('30', '13', 'Juan Dela Cruz has login.', '2020-05-07 00:00:00', null);
INSERT INTO `tbl_log` VALUES ('31', '13', 'Juan, Dela Cruz has  Pending Calter, Nicolas S.\'s request.', '2020-05-07 00:00:00', null);
INSERT INTO `tbl_log` VALUES ('32', '13', 'Juan Dela Cruz has login.', '2020-05-08 00:00:00', null);
INSERT INTO `tbl_log` VALUES ('33', '13', 'Juan Dela Cruz has login.', '2020-05-16 00:00:00', null);
INSERT INTO `tbl_log` VALUES ('34', '14', 'Jose Rizal has login.', '2020-05-16 00:00:00', null);
INSERT INTO `tbl_log` VALUES ('35', '20', 'Nicolas Calter has login.', '2020-05-16 00:00:00', null);

-- ----------------------------
-- Table structure for tbl_profile
-- ----------------------------
DROP TABLE IF EXISTS `tbl_profile`;
CREATE TABLE `tbl_profile` (
  `profile_id` int(255) NOT NULL AUTO_INCREMENT,
  `generated_id` varchar(255) DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `mname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `blood_type` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `disability_type` varchar(255) DEFAULT NULL,
  `guardian_name` varchar(255) DEFAULT NULL,
  `guardian_number` varchar(255) DEFAULT NULL,
  `profile_type` int(1) DEFAULT '1',
  `date_registered` date DEFAULT NULL,
  `date_deleted` date DEFAULT NULL,
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_profile
-- ----------------------------
INSERT INTO `tbl_profile` VALUES ('13', null, 'Juan', 'L.', 'Dela Cruz', '2019-09-01', 'MALE', '09555216863', null, 'Calao East', 'None', null, null, '1', '2019-09-05', null);
INSERT INTO `tbl_profile` VALUES ('14', null, 'Jose', 'P.', 'Rizal', '2019-01-01', 'MALE', '09555216863', 'undefined', 'Calao East', 'undefined', '', '', '1', '2019-09-05', null);
INSERT INTO `tbl_profile` VALUES ('15', null, 'Apol', 'Y.', 'Mabini', '2011-01-01', 'MALE', null, 'A+', 'Calao East', null, null, null, '1', '2019-09-05', null);
INSERT INTO `tbl_profile` VALUES ('16', '16-8026-441-1473', 'Jeffrey Luis', 'A.', 'Guya', '2019-01-01', 'MALE', '09187421273', 'AB ', 'Centro West', 'Orthopedic (Musculoskeletal) Disability', 'Bill Gates', '12345555555', '1', '2019-09-05', null);
INSERT INTO `tbl_profile` VALUES ('18', '17-2208-120-1845', 'JL', 'S.', 'Barcelona', '2019-01-01', 'MALE', '09555216863', 'AB ', 'Abra', 'Orthopedic (Musculoskeletal) Disability', 'Bill Gates', '12345555555', '1', '2019-09-09', null);
INSERT INTO `tbl_profile` VALUES ('19', '19-3620-362-2351', 'sdasdasdsad', 'asdasdasdad', 'asdadasd', '2019-05-09', 'MALE', '12312312312', 'AB ', 'Cabulay', 'Orthopedic (Musculoskeletal) Disability', '', '', '1', '2020-03-06', null);
INSERT INTO `tbl_profile` VALUES ('20', '20-4546-690-7424', 'Nicolas', 'S.', 'Calter', '2020-03-19', 'MALE', '09555216863', 'AB-', 'Centro West', 'Psychosocial Disability', 'asdasdasdasdasdasdasd', '13123123123', '1', '2020-03-09', null);
INSERT INTO `tbl_profile` VALUES ('21', '21-8488-793-2934', 'Junmar', 'S.', 'Fajardo', '2020-01-01', 'MALE', '09754952519', 'AB ', 'Ambalatungan', 'Mental Disability', 'cvbnm,.jkl', '34567897978', '1', '2020-03-14', null);

-- ----------------------------
-- Table structure for tbl_request
-- ----------------------------
DROP TABLE IF EXISTS `tbl_request`;
CREATE TABLE `tbl_request` (
  `request_id` int(255) NOT NULL AUTO_INCREMENT,
  `profile_id` int(255) DEFAULT NULL,
  `date_request` date DEFAULT NULL,
  `request_type` varchar(255) DEFAULT NULL,
  `approved` int(1) DEFAULT '0',
  `claimant_name` varchar(255) DEFAULT NULL,
  `date_registered` date DEFAULT NULL,
  `date_deleted` date DEFAULT NULL,
  `date_claimed` date DEFAULT NULL,
  PRIMARY KEY (`request_id`),
  KEY `request_account` (`profile_id`),
  CONSTRAINT `request_account` FOREIGN KEY (`profile_id`) REFERENCES `tbl_profile` (`profile_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_request
-- ----------------------------
INSERT INTO `tbl_request` VALUES ('7', '16', '2019-09-27', 'New', '2', 'Testing', '2019-09-19', null, '2020-03-27');
INSERT INTO `tbl_request` VALUES ('8', '20', '2020-03-16', 'New', '0', 'Greg papa pogi', '2020-03-15', null, '2020-03-15');

-- ----------------------------
-- Table structure for tbl_requirements
-- ----------------------------
DROP TABLE IF EXISTS `tbl_requirements`;
CREATE TABLE `tbl_requirements` (
  `file_id` int(255) NOT NULL AUTO_INCREMENT,
  `profile_id` int(255) DEFAULT NULL,
  `file_name` longtext,
  `file_path` longtext,
  `file_type` varchar(255) DEFAULT NULL,
  `file_size` double(255,0) DEFAULT NULL,
  `is_photo` int(1) DEFAULT '0',
  `date_registered` date DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `requirements` (`profile_id`),
  CONSTRAINT `requirements` FOREIGN KEY (`profile_id`) REFERENCES `tbl_profile` (`profile_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_requirements
-- ----------------------------
INSERT INTO `tbl_requirements` VALUES ('18', '18', 'user.PNG', '5d75e5bd5a29e9.68648249.png', 'image/png', '10141', '1', '2019-09-09');
INSERT INTO `tbl_requirements` VALUES ('19', '18', '62.pdf', '5d75e64ae6f0b4.07086578.pdf', 'application/pdf', '134079', '0', '2019-09-09');
INSERT INTO `tbl_requirements` VALUES ('29', '16', '17795901_1478253198874396_2380371579542396204_n.jpg', '5d83211407d6d1.91993422.jpg', 'image/jpeg', '56123', '1', '2019-09-19');
INSERT INTO `tbl_requirements` VALUES ('30', '16', '399px-Naomi_Scott.jpg', '5d832123620e88.71509366.jpg', 'image/jpeg', '57537', '0', '2019-09-19');
INSERT INTO `tbl_requirements` VALUES ('31', '16', '711px-PHP-logo.svg.png', '5d8321237c0178.11829338.png', 'image/png', '41313', '0', '2019-09-19');
INSERT INTO `tbl_requirements` VALUES ('33', '20', '2a2cdc1cb84b678eaf788e89edf56110.jpg', '5e6e3d4377ea17.04727593.jpg', 'image/jpeg', '470361', '0', '2020-03-15');
INSERT INTO `tbl_requirements` VALUES ('34', '20', '487bafae3b7b99439c8fc156d8866cbb-engine-piston-flat-illustration.jpg', '5e6e3d4a2d95b2.72715014.jpg', 'image/jpeg', '45103', '0', '2020-03-15');
INSERT INTO `tbl_requirements` VALUES ('35', '20', '23119958_1689705091062538_4466783911851427058_o.jpg', '5ebfcb43e3f8f4.90797639.jpg', 'image/jpeg', '48579', '1', '2020-05-16');
