var url = '../actions.php';

$(document).ready(function(){
	notification_refresh();
});

function notification_refresh(){
	notification_alert();
	notification_alert_count();
}


function load_dashboard(){
  dashboard_count('claim','claim_count');
  dashboard_count('disapprove','disapprove_count');
  dashboard_count('pending','pending_count');
  dashboard_count('approve','approve_count');
  show_claimed_id();
}

setInterval(function(){
  dashboard_count('claim','claim_count');
  dashboard_count('disapprove','disapprove_count');
  dashboard_count('pending','pending_count');
  dashboard_count('approve','approve_count');
  notification_alert_count();
  // console.log('testing');
},4000);


   function dashboard_count(types,id){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=dash_count' + '&types=' + types,
      cache:false,
      success:function(data){
        $("#"+id).text(data.trim());
      }
    });
  }

    function print_page(){
          var gender_filter = $("#gender_filter");
          var address_filter = $("#address_filter");
          var filter_name = $("#filter_name").val();

           window.open("print_pdf.php?address_filter="+address_filter.val()+"&gender_filter="+gender_filter.val()+"&filter_name="+filter_name+'&disab_filter=' + $("#disab_filter").val());       
        }

    function print_page_pdf(type){
      var gender_filter = $("#gender_filter");
      var address_filter = $("#address_filter");
      var filter_name = $("#filter_name").val();

       window.open("print_pdf_request.php?address_filter="+address_filter.val()+"&gender_filter="+gender_filter.val()+"&filter_name="+filter_name+'&disab_filter=' + $("#disab_filter").val()+'&type=' + type);       
    }

   function show_pwd(){
    var gender_filter = $("#gender_filter").val();
    var address_filter = $("#address_filter").val();
    var filter_name = $("#filter_name").val();
    var disab_filter = $("#disab_filter").val();


    var mydata = '&gender_filter=' + gender_filter + '&address_filter=' + address_filter + '&filter_name=' + filter_name + '&disab_filter=' + disab_filter;


    $.ajax({
      type:"POST",
      url:url,
      data:'action=showPwd' + mydata,
      cache:false,
      success:function(data){
        // alert(data);
          $("#pwd_data").html(data);
          $('#tbl_pwd').DataTable();
      }
    });
  }

 function notification_alert(){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=notifications',
      cache:false,
      success:function(data){
        $("#notification_data").html(data);
      }
    });
  }

   function notification_alert_count(){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=alert_count',
      cache:false,
      success:function(data){
        $("#counter_alert").text(data.trim());
      }
    });
  }


  function show_all_request(type){
     var gender_filter = $("#gender_filter").val();
    var address_filter = $("#address_filter").val();
    var filter_name = $("#filter_name").val();
    var disab_filter = $("#disab_filter").val();


    var mydata = '&gender_filter=' + gender_filter + '&address_filter=' + address_filter + '&filter_name=' + filter_name + '&disab_filter=' + disab_filter;



    $.ajax({
      type:"POST",
      url:url,
      data:'action=show_pending' + '&type=' + type + mydata,
      cache:false,
      success:function(data){
        $("#pending_data").html(data);
        $("#tbl_pending").DataTable();
      }
    });
  }

  function delete_claimed(id){
    swal({
        title: "Are you sure?",
        text: "Do you want to delete this record?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
      function(){
        $.ajax({
          type:"POST",
          url:url,
          data:'action=delete_claimed' + '&id=' + id,
          cache:false,
          success:function(data){
            if (data.trim() == 1) {
               swal("Success","Profile has been deleted!","success");
               show_claimed_id();
               setTimeout(function(){
                location.reload();
               },3000);
            }else{
              console.log(data);
            }
          }
        });
      });
  }

  function delete_account(id, name){
  swal({
      title: "Are you sure?",
      text: "Do you want to delete "+name+"?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
        type:"POST",
        url:url,
        data:'action=delete_account' + '&id=' + id + '&r_name=' + name,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
             show_pwd();
             show_staff();
             swal("Success","Profile has been deleted!","success");
          }else{
            console.log(data);
          }
        }
      });
    });
}

function show_staff(){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=showStaff',
      cache:false,
      success:function(data){
        // alert(data);
          $("#staff_data").html(data);
          $('#tbl_staff').DataTable();
      }
    });
  }

   function show_claimed_id(){
          $.ajax({
            type:"POST",
            url:url,
            data:'action=claimed_id',
            cache:false,
            success:function(data){
              $("#claim_data").html(data);
              $("#tbl_claim").DataTable();

            }
          });
        }

  function approve_disapprove(name,id, type, display, number){
  var set_val = 0;
  var msg = "Do you want to  "+type +' '+name+" ?";

  if (type == "approve") {
    set_val = 1;
  }else if (type == "disapprove") {
    set_val = 3;
  }else if (type == "pending") {
    set_val = 0;
    msg = "Do you want to set "+name+" as pending ?";
  }else if (type == "claim") {
    set_val = 2;
     msg = "Do you want to set "+name+" as claim ?";
  }
  else if (type == "printed") {
     set_val = 2;
     msg = "Do you want to set "+name+" as claim ?";
  }

  swal({
      title: "Are you sure?",
      text: msg,
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#4e73df",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
        type:"POST",
        url:url,
        data:'action=approve_disapprove_request' + '&id=' + id + '&set_val=' + set_val + '&r_name=' + name + '&number=' + number,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
             swal("Success","Request has been "+type+"!","success");
             show_all_request(display);
             notification_refresh();
             load_dashboard();

             setTimeout(function(){
              location.reload();
             },2000);
          }else{
            console.log(data);
          }
        }
      });
    });
}


function claim(name,id, type, display){
  var set_val = 0;
  var msg = "Do you want to  "+type +' '+name+" ?";

  if (type == "approve") {
    set_val = 1;
  }else if (type == "disapprove") {
    set_val = 3;
  }else if (type == "pending") {
    set_val = 0;
    msg = "Do you want to set "+name+" as pending ?";
  }else if (type == "claim") {
    set_val = 2;
     msg = "Do you want to set "+name+" as claim ?";
  }
  else if (type == "printed") {
     set_val = 2;
     msg = "Do you want to set "+name+" as claim ?";
  }


  swal({
  title: "Are you sure?",
  text: "Do you want to claim "+name+" ID?",
  type: "input",
  showCancelButton: true,
  closeOnConfirm: false,
  inputPlaceholder: "Enter Claimant name"
  }, function (inputValue) {
    if (inputValue === false) return false;
    if (inputValue === "") {
      swal.showInputError("You need to write something!");
      return false
    }

    $.ajax({
      type:"POST",
      url:url,
      data:'action=claim_request' + '&id=' + id + '&set_val=' + set_val + '&claimant=' + inputValue + '&r_name=' + name,
      cache:false,
      success:function(data){
        if (data.trim() == 1) {
           swal("Success","Id has been successfully claimed!","success");
           show_all_request(display);
           notification_refresh();
           load_dashboard();

           setTimeout(function(){
            location.reload();
           },2000);
        }else{
          console.log(data);
        }
      }
    });
    // swal("Nice!", "You wrote: " + inputValue, "success");


  });
}

 function show_requirements_details(id, name){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=requirements' + '&id=' + id,
      cache:false,
      success:function(data){
        // alert(id);
          $("#view_files").html(data);
          $("#name_requirements").text(name);
          $('#requirementTable').DataTable();
          $("#requirements_modal").modal('show');
      }
    });
  }


function edit_clear_admin(id,fn,mn,ln,gender,disability,address,email,gname,contacts,cp_number,bdate,blood_type){
    $("#profile_id").val(id);
    $("#fn").val(fn);
    $("#mn").val(mn);
    $("#ln").val(ln);
    $("#gender").val(gender);
    $("#disability").val(disability);
    $("#address").val(address);
    $("#email").val(email);
    $("#gname").val(gname);
    $("#contacts").val(contacts);
    $("#cp_number").val(cp_number);
    $("#bdate").val(bdate);
    $("#blood_type").val(blood_type);

  }



   
  function register_walkIn_staff(status){
    var id = $("#profile_id");
    var fn = $("#fn");
    var mn = $("#mn");
    var ln = $("#ln");
    var gender = $("#gender");
    var disability = $("#disability");
    var address = $("#address");
    var email = $("#email");
    var gname = $("#gname");
    var contacts = $("#contacts");
    var cp_number = $("#cp_number");
    var bdate = $("#bdate");
    var blood_type = $("#blood_type");


    if (fn.val() == "") {
       alerts('Enter firstname!','','','','','','','','','','','','','');
        fn.focus();
    }
    else if (mn.val() == "") {
      mn.focus();
       alerts('','Enter middlename!','','','','','','','','','','','','');
    }
    else if (ln.val() == "") {
      ln.focus();
       alerts('','','Enter lastname!','','','','','','','','','','','');
    }
    else if (bdate.val() == "") {
      bdate.focus();
       alerts('','','','','','','','','','Enter birthdate','','','','');
    }
    else if (gender.val() == null) {
      gender.focus();
       alerts('','','','Select gender!','','','','','','','','','','');
    }
    else if (blood_type.val() == null && status != 2) {
      blood_type.focus();
       alerts('','','','','','','','','','','','','Select blood type!','');
    }
    else if (disability.val() == "" && status != 2) {
      disability.focus();
      alerts('','','','','Enter disablity!','','','','','','','','','');
    }
    else if (address.val() == "") {
      address.focus();
       alerts('','','','','','Enter address!','','','','','','','','');
    }
    else if (email.val() == "") {
      email.focus();
       alerts('','','','','','','Enter e-mail address!','','','','','','','');
    }
    else if (!validateEmail(email.val())) {
      email.focus();
      alerts('','','','','','','Enter valid e-mail address \'@\'/.com !','','','','','','','');
    }
   else if (cp_number.val() == "") {
      cp_number.focus();
       alerts('','','','','','','','','','','','','','Enter your contact number!');
    }
    else if (gname.val() == "" && status != 2) {
      gname.focus();
      alerts('','','','','','','','Enter contact name in case of emergency!','','','','','','');
    }
    else if (contacts.val() == "" && status != 2) {
      contacts.focus();
      alerts('','','','','','','','','Enter contact number in case of emergency!','','','','','');
    }
    else{
      var mydata = 'action=register_walkin' + '&fn=' + fn.val() + '&mn=' + mn.val() + '&ln=' + ln.val() + '&gender=' + gender.val() + '&disability=' + disability.val() + '&address=' + address.val() + '&email=' + email.val() + '&gname=' + gname.val() + '&contacts=' + contacts.val() + '&bdate=' + bdate.val() + '&status=' + status + '&profile_id=' + id.val() + '&cp_number=' + cp_number.val() + '&blood_type=' + blood_type.val();
      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        success:function(data){
          console.log(data);

          if (data == 1) {
             $(".close").click();
             $("#btn_submit").attr('disabled', false);
             swal("Success","Profile has been saved!", "success");
             edit_clear_admin('','','','','','','','','','','');
             alerts('','','','','','','','','','','','','','');
             $("#cp_number").val('');
            $("#blood_type").val('');
             show_pwd();
             show_staff();
          }else if(data.trim() == 2){
            email.focus();
            swal("Sorry","Email address is already exist!", "error");
             alerts('','','','','','','E-mail address is already exist!','','','','','','','');
          }
        }
      });

    }
  }