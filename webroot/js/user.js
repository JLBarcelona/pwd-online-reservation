 var url = '../actions.php';

show_profile_picture();

 function show_files(){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=file_manager',
      cache:false,
      success:function(data){
        // alert(data);
          $("#file_data").html(data);
          $('#fileTable').DataTable();
      }
    });
  }

  function show_request(){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=show_request',
      cache:false,
      success:function(data){
        $("#request_data").html(data);
        $("#request_table").DataTable();
      }
    });
  }


function delete_js(name,id, path){
  swal({
      title: "Are you sure?",
      text: "Do you want to delete "+name+" ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
        type:"POST",
        url:url,
        data:'action=delete_file' + '&id=' + id + '&path=' + path,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
             show_files();
             swal("Success","File has been deleted!","success");
             show_profile_picture();
          }else{
            console.log(data);
          }
        }
      });
    });
}


function delete_record(id,status){
  swal({
      title: "Are you sure?",
      text: "Do you want to delete your request?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
        type:"POST",
        url:url,
        data:'action=delete_request' + '&id=' + id + '&status=' + status,
        cache:false,
        success:function(data){
          if (data.trim() == 1) {
             show_request();
             swal("Success","Request has been deleted!","success");
          }else{
            console.log(data);
          }
        }
      });
    });
}

function show_profile_picture(){
  $.ajax({
    type:"POST",
    url:url,
    data:'action=profile_picture',
    cache:false,
    success:function(data){
      $(".my_avatar").attr("src",data.trim());
    }
  });
}


 $("#upload_form").on('submit', function(e){
      e.preventDefault();

      $.ajax({
        type:"POST",
        url:'upload.php',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        beforeSend:function(){
          $("#up_btn").attr('disabled', true);
        },
        success:function(data){
          // alert(data);
          if (data.trim() == 1 || data.trim() == 11) {
            swal("Success","File uploaded!","success");
            $("#files").val('');
            show_profile_picture();
            show_files();
          }else if (data.trim() == 505 || data.trim() == 505505) {
            swal("Sorry","you can\'t upload more than 3 files!","error");
            $("#files").val('');
          }else if (data.trim() == 202) {
            swal("Sorry","One of the requirements should be 2x2 picture!","error");
            $("#files").val('');
          }else if (data.trim() == 200) {
            swal("Sorry","you can\'t upload more than one 2x2 pictures!","error");
          }else{
            swal("Sorry","Upload error try again!","error");
          }
          $("#up_btn").attr('disabled', false);
        }
      });
    });


    $("#files").on('change', function(e){
    e.preventDefault();
        // var fullPath = this.value;
        // if (fullPath != "" || fullPath != null) {
        //   $("#up_btn").attr('disabled', false);
        //   var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
        //     var filename = fullPath.substring(startIndex);
        //     if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
        //         filename = filename.substring(1);
        //     }
        //     //get file extension 
        //     var extensions = filename.split('.').pop();
            
        //     if(extensions == 'pdf' || extensions == 'png' || extensions == 'jpg' || extensions == 'jpeg' || extensions == 'PNG' || extensions == 'JPG' || extensions == 'JPEG'){
        //        return true;
        //     }
        //     else{
        //       swal("Oops!","You can\'t upload this kind of file !","error");
        //       return false;
        //     }


        // }else{
         
        // }
         // $("#up_btn").attr('disabled', true);
    });



    function validation_request(alert_duedate,alert_request){
      $("#alert_duedate").text(alert_duedate);
      $("#alert_request").text(alert_request);
    }

    function make_request(){
      var due_date = $("#due_date");
      var type_request = $("#type_request");
      var id_number =   $("#id_number");


        if (id_number.val() == "") {
          id_number.focus();
          $("#alert_idnumber").text('Please enter your ID Number!');
        }
        else if (due_date.val() == "") {
          due_date.focus();
          validation_request('Enter your sechedule!','');
           $("#alert_idnumber").text('');
        }
        else if (type_request.val() == "" || type_request.val() == null) {
          type_request.focus();
          validation_request('','Select your request!');
           $("#alert_idnumber").text('');
        }
        else{
          var mydata = 'action=save_request' + '&due_date=' + due_date.val() + '&type_request=' + type_request.val()  + '&id_number=' + id_number.val();
          $.ajax({
            type:"POST",
            url:url,
            data:mydata,
            cache:false,
            success:function(data){
              if (data.trim() == 123) {
                swal("Sorry","Invalid date schedule!","error");
                 due_date.focus();
              }else if (data.trim() == 301) {
                swal("Sorry","You cant schedule during weekends!","error");
                 due_date.focus();
              }else if (data.trim() == 1) {
                swal("Success","Request has been sent successfully!","success");
                 validation_request('','');
                  $("#alert_idnumber").text('');
                 due_date.val('');
                 type_request.val('');
                 show_request();
              }else if (data.trim() == 101) {
                 swal("Sorry","You already have 1 request please wait until your request is approve!","error");
              }else if (data.trim() == 404) {
                swal("Sorry","Your ID number format is invalid!","error");
                id_number.focus();
              }else if (data.trim() == 202) {
                swal("Sorry","Please upload your 2x2 picture!","error");
              }else{
                console.log(data.trim());
              }
            }
          });
        }

    }