var url = 'actions.php';

function alert_login(alert_email_add,alert_pwd){
  $("#alert_email_add").text(alert_email_add);
  $("#alert_pwd").text(alert_pwd);
}

function login(){
  var email_add = $("#email_add");
  var pwd = $("#pwd");

  if (email_add.val() == "") {
    email_add.focus();
    alert_login('Enter Email address!','');
  }
  else if (!validateEmail(email_add.val())) {
    email_add.focus();
    alert_login('Enter valid e-mail address \'@\'/.com !','');
  }
  else if (pwd.val() == "") {
    pwd.focus();
    alert_login('','Enter password!');
  }else{
    var mydata = 'action=login' + '&email_add=' + email_add.val() + '&pwd=' + pwd.val();
    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      beforeSend:function(){
        // $("#btn_log").attr('disabled', true);
      },
      success:function(data){
        // alert(data.trim());
        if (data.trim() == 1) {
          // admin
          $("#btn_log").attr('disabled', false);
          email_add.val('');
          pwd.val('');
          window.location = "admin/";
        }else if (data.trim() == 2) {
          // staff
          $("#btn_log").attr('disabled', false);
          email_add.val('');
          pwd.val('');
          window.location = "admin/";
        }else if (data.trim() == 3) {
          // user
          $("#btn_log").attr('disabled', false);
          email_add.val('');
          pwd.val('');
          window.location = "user/";
        }else{
           swal("Oops!","Username or password is incorrect!","error");
           console.log(data.trim());
        }
      }
    });
  }

}

  function alphanumeric() { 
    var txt = document.getElementById("password");
    var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    if(txt.value.match(passw)) { 
      // alert('Correct, try another...')
      return true;
    }
    else{ 
      // alert('Wrong...!')
      return false;
    }
  } 

  function register(){
    var fn = $("#fn");
    var mn = $("#mn");
    var ln = $("#ln");
    var gender = $("#gender");
    var blood_type = $("#blood_type");
    var disability = $("#disability");
    var address = $("#address");
    var email = $("#email");
    var gname = $("#gname");
    var contacts = $("#contacts");
    var cp_number = $("#cp_number");
    var bdate = $("#bdate");
    var password = $("#password");
    var cpassword = $("#cpassword");


    if (fn.val() == "") {
       alerts('Enter firstname!','','','','','','','','','','','','','');
        fn.focus();
    }
    else if (mn.val() == "") {
      mn.focus();
       alerts('','Enter middlename!','','','','','','','','','','','','');
    }
    else if (ln.val() == "") {
      ln.focus();
       alerts('','','Enter lastname!','','','','','','','','','','','');
    }
    else if (bdate.val() == "") {
      bdate.focus();
       alerts('','','','','','','','','','Enter birthdate','','','','');
    }
    else if (gender.val() == null) {
      gender.focus();
       alerts('','','','Select gender!','','','','','','','','','','');
    }
    else if (blood_type.val() == null) {
      blood_type.focus();
       alerts('','','','','','','','','','','','','Select blood type!','');
    }
    else if (disability.val() == "") {
      disability.focus();
      alerts('','','','','Enter disablity!','','','','','','','','','');
    }
    else if (address.val() == "") {
      address.focus();
       alerts('','','','','','Enter address!','','','','','','','','');
    }
    else if (email.val() == "") {
      email.focus();
       alerts('','','','','','','Enter e-mail address!','','','','','','','');
    }
    else if (!validateEmail(email.val())) {
      email.focus();
      alerts('','','','','','','Enter valid e-mail address \'@\'/.com !','','','','','','','');
    }
   else if (cp_number.val() == "") {
      cp_number.focus();
       alerts('','','','','','','','','','','','','','Enter your contact number!');
    }
    else if (password.val() == "") {
      password.focus();
      alerts('','','','','','','','','','','Enter pasword!','','','');
    }
    else if (alphanumeric() == false) {
      swal("Oops!","Password Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters","error");
      password.focus();
    }
    else if (cpassword.val() == "") {
      cpassword.focus();
      alerts('','','','','','','','','','','','Confirm password!','','');
    }
    else if (cpassword.val() != password.val()) {
      cpassword.focus();
      alerts('','','','','','','','','','','','Password doestn\'t match!','','');
    }
    else if (gname.val() == "") {
      gname.focus();
      alerts('','','','','','','','Enter contact name in case of emergency!','','','','','','');
    }
    else if (contacts.val() == "") {
      contacts.focus();
      alerts('','','','','','','','','Enter contact number in case of emergency!','','','','','');
    }
    else{

      var mydata = 'action=register' + '&fn=' + fn.val() + '&mn=' + mn.val() + '&ln=' + ln.val() + '&gender=' + gender.val() + '&disability=' + disability.val() + '&address=' + address.val() + '&email=' + email.val() + '&gname=' + gname.val() + '&contacts=' + contacts.val() + '&bdate=' + bdate.val() + '&password=' + password.val() + '&blood_type=' + blood_type.val() + '&cp_number=' + cp_number.val();
      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        beforeSend:function(){
          // $("#btn_submit").attr('disabled', true);
           swal("Please Wait","You will receive an sms for your account verification code!", "info");
        },
        success:function(data){
          console.log(data);
          if (data == 1) {
             $("#btn_submit").attr('disabled', false);
             swal("Success","Please check your inbox to get your verification!", "success");
             setTimeout(function(){
              window.location ="verification.php";
             },2000);
             edit_clear('','','','','','','','','','','','','','');
             alerts('','','','','','','','','','','','','','');
          }else if(data.trim() == 2){
            email.focus();
            swal("Sorry","Email address is already exist!", "error");
            alerts('','','','','','','Email address is already exist!','','','','','');
          }
        }
      });

    }
  }