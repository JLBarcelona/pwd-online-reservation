  
  function save_profile(){
    var fn = $("#fn");
    var mn = $("#mn");
    var ln = $("#ln");
    var gender = $("#gender");
     var blood_type = $("#blood_type");
    var disability = $("#disability");
    var address = $("#address");
    var gname = $("#gname");
    var contacts = $("#contacts");
    var bdate = $("#bdate");





    if (fn.val() == "") {
       alerts('Enter firstname!','','','','','','','','','','','','');
        fn.focus();
    }
    else if (mn.val() == "") {
      mn.focus();
       alerts('','Enter middlename!','','','','','','','','','','','');
    }
    else if (ln.val() == "") {
      ln.focus();
       alerts('','','Enter lastname!','','','','','','','','','','');
    }
    else if (bdate.val() == "") {
      bdate.focus();
       alerts('','','','','','','','','','Enter birthdate','','','');
    }
    else if (gender.val() == null) {
      gender.focus();
       alerts('','','','Select gender!','','','','','','','','','');
    }
    else if (blood_type.val() == null) {
      blood_type.focus();
       alerts('','','','','','','','','','','','','Select blood type!');
    }
    else if (disability.val() == "") {
      disability.focus();
      alerts('','','','','Enter disablity!','','','','','','','','');
    }
    else if (address.val() == "") {
      address.focus();
       alerts('','','','','','Enter address!','','','','','','','');
    }
    else if (gname.val() == "") {
      gname.focus();
      alerts('','','','','','','','Enter contact name in case of emergency!','','','','','');
    }
    else if (contacts.val() == "") {
      contacts.focus();
      alerts('','','','','','','','','Enter contact number in case of emergency!','','','','');
    }
    else{
      var mydata = 'action=change_profile' + '&fn=' + fn.val() + '&mn=' + mn.val() + '&ln=' + ln.val() + '&gender=' + gender.val() + '&disability=' + disability.val() + '&address=' + address.val() + '&gname=' + gname.val() + '&contacts=' + contacts.val() + '&bdate=' + bdate.val() + '&blood_type=' + blood_type.val();
      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        success:function(data){
          console.log(data);
          if (data == 1) {
             swal("Success","Profile has been updated!", "success");
             set_session();
             alerts('','','','','','','','','','','','');
          }else{
            console.log(data);
          }
        }
      });

    }
  }



  function save_account_email(){
  
    var email = $("#email");
  
    if (email.val() == "") {
      email.focus();
       alerts('','','','','','','Enter e-mail address!','','','','','');
    }
    else if (!validateEmail(email.val())) {
      email.focus();
      alerts('','','','','','','Enter valid e-mail address \'@\'/.com !','','','','','');
    }
    else{
      var mydata = 'action=change_email' + '&email=' + email.val();
      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        success:function(data){
          console.log(data);
          if (data == 1) {
             swal("Success","Email has been saved!", "success");
             alerts('','','','','','','','','','','','');
             set_session();
          }else if(data.trim() == 2){
            email.focus();
            swal("Sorry","Email address is already exist!", "error");
            alerts('','','','','','','Email address is already exist!','','','','','');
          }
        }
      });

    }
  }


  function save_new_password(){
    var o_password = $("#o_password");
    var password = $("#password");
    var cpassword = $("#cpassword");


    if (o_password.val() == "") {
      o_password.focus();
      $("#o_password_alert").text('Enter your old password!');
    }
    else if (password.val() == "") {
      password.focus();
      alerts('','','','','','','','','','','Enter pasword!','');
       $("#o_password_alert").text('');
    }
    else if (cpassword.val() == "") {
      cpassword.focus();
      alerts('','','','','','','','','','','','Confirm password!');
       $("#o_password_alert").text('');
    }
    else if (cpassword.val() != password.val()) {
      cpassword.focus();
      alerts('','','','','','','','','','','','Password doestn\'t match!');
       $("#o_password_alert").text('');
    }
    else{
      var mydata = 'action=change_password'  + '&o_password=' + o_password.val() + '&password=' + password.val();
      $.ajax({
        type:"POST",
        url:url,
        data:mydata,
        cache:false,
        success:function(data){
          // console.log(data);
          if (data.trim() == 1) {
             swal("Success","Password has been changed!", "success");
             alerts('','','','','','','','','','','','');
             $("#o_password_alert").text('');
              o_password.val('');
              password.val('');
              cpassword.val('');
             set_session();
          }else if (data.trim() == 404) {
             swal("Sorry","Password is incorrect!", "error");
            $("#o_password_alert").text('Password is incorrect!');
          }else {
            console.log(data);
          }
        }
      });

    }
  }

  function set_session(){
    $.ajax({
      type:"POST",
      url:url,
      data:{action:'get_sessions'},
      dataType:'json',
      success:function(data){
        // alert(data.birthdate);
         $("#fn").val(data.fname);
         $("#mn").val(data.mname);
         $("#ln").val(data.lname);
         $("#gender").val(data.gender);
         $("#disability").val(data.disability_type);
         $("#address").val(data.address);
         $("#email").val(data.email_address);
         $("#gname").val(data.guardian_name);
         $("#contacts").val(data.guardian_number);
         $("#bdate").val(data.birthdate);
      }
    });
  }