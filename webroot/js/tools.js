
 function validateEmail(emails) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(emails);
}


function tool(){
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip(); 
	});
}




function edit_clear(fn,mn,ln,gender,disability,address,email,gname,contacts,bdate,password,cpassword,blood_type,cp_number){
     $("#fn").val(fn);
     $("#mn").val(mn);
     $("#ln").val(ln);
     $("#gender").val(gender);
     $("#disability").val(disability);
     $("#address").val(address);
     $("#email").val(email);
     $("#gname").val(gname);
     $("#contacts").val(contacts);
     $("#bdate").val(bdate);
     $("#password").val(password);
     $("#cpassword").val(cpassword);
     $("#blood_type").val(blood_type);
      $("#cp_number").val(cp_number);
  }


 function alerts(fn_alert,mn_alert,ln_alert,gender_alert,dis_alert,address_alert,email_alert,gname_alert,contact_alert,date_alert,password_alert,cpassword_alert,blood_type,cp_number){
    $("#fn_alert").text(fn_alert);
    $("#mn_alert").text(mn_alert);
    $("#ln_alert").text(ln_alert);
    $("#gender_alert").text(gender_alert);
    $("#dis_alert").text(dis_alert);
    $("#address_alert").text(address_alert);
    $("#email_alert").text(email_alert);
    $("#gname_alert").text(gname_alert);
    $("#contact_alert").text(contact_alert);
    $("#date_alert").text(date_alert);
    $("#password_alert").text(password_alert);
    $("#cpassword_alert").text(cpassword_alert);
    $("#blood_type_alert").text(blood_type);
    $("#cp_number_alert").text(cp_number);
  }


  function num_only(evt){
	var charcode= (evt.which) ? evt.which : event.keycode
	if (charcode > 31 && (charcode < 48 ||  charcode >57)){
	 	alerts('','','','','','','','','Enter number only!','');
		return false;
	}
	else
	{
		alerts('','','','','','','','','','');
		return true;
	}	
}


