<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>
<style type="text/css">
  .dataTables_filter{
    display: none;
  }
  .dataTables_length{
    display: none;
  }
  .text-boot{
    height: 30px;
    width: auto;
    border-radius: 5px;
    border-color: #c1c1c1;
    border:solid 1px #c1c1c1;
    /*outline-color: transparent; */
    transition: outline-color 0.2s;
    padding: 3px;
  }
</style>
<body id="page-top" onload="show_all_request(0)">

  <!-- Page Wrapper -->
  <div id="wrapper">

      <?php include('sidenav.php'); ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Pending Request</h1>
          </div>

          <!-- Content Row -->

          <!-- Content Row -->
           <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-12">
               <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->

                 <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary ">LIST OF PENDING REQUEST</h6>
                  <div class="dropdown no-arrow">
                    <select name="gender_filter" id="gender_filter" class="text-boot">
                      <option value="">Select Gender</option>
                      <option>MALE</option>
                      <option>FEMALE</option>
                    </select>


                    <select name="disab_filter" id="disab_filter" class="text-boot">
                      <option value="">Select Disability</option>
                      <option>Psychosocial Disability</option>
                      <option>Mental Disability</option>
                      <option>Hearing Disability</option>
                      <option>Visial Disability</option>
                      <option>Speech impairment</option>
                      <option>Learning Disability</option>
                      <option>Orthopedic (Musculoskeletal) Disability</option>
                    </select>

                    <select name="address_filter" id="address_filter" class="text-boot">
                      <option value="">Select Address</option>
                       <?php echo select_address(); ?>
                    </select>

                    <input type="text" name="filter_name" id="filter_name" class="text-boot" placeholder="Search Name or ID">

                    <button class="btn btn-sm btn-dark" onclick="show_all_request(0)"><i class="fa fa-search"></i></button>
                    <button class="btn btn-sm btn-primary" onclick="print_page_pdf(0);"><i class="fa fa-print"></i></button>

                    <a class="btn btn-sm btn-success" style="display: none;" href="#" role="button" aria-haspopup="true" aria-expanded="false" data-toggle="modal" data-backdrop="static" data-target="#registerModal">
                      <i class="fas fa-plus fa-sm fa-fw text-white-400"></i>
                    </a>
                   <!--  <a class="dropdown-toggle btn btn-sm btn-dark" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-white-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Option</div>
                      <a class="dropdown-item" href="#"><i class="fa fa-print"></i> Print</a>
                    </div> -->
                  </div>
                </div>

                <!-- Card Body -->
                <div class="card-body">
                  <div class="table-responsive">
                <table class="table table-bordered" id="tbl_pending" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Disability</th>
                      <th>Request Type</th>
                      <th>Date Entry</th>
                      <th>Due Date</th>
                      <th>Option</th>
                    </tr>
                  </thead>
                  <tbody id="pending_data">
                  </tbody>
                </table>
              </div>
                </div>
              </div>

            </div>
          </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->

      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

<?php 
include('footer.php');  
include('modal.php'); 
?>
</body>

</html>
