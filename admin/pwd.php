<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>
<body id="page-top" onload="show_pwd()">
<style type="text/css">
  .dataTables_filter{
    display: none;
  }
  .dataTables_length{
    display: none;
  }
  .text-boot{
    height: 30px;
    width: auto;
    border-radius: 5px;
    border-color: #c1c1c1;
    border:solid 1px #c1c1c1;
    /*outline-color: transparent; */
    transition: outline-color 0.2s;
    padding: 3px;
  }
</style>
  <!-- Page Wrapper -->
  <div id="wrapper">

      <?php include('sidenav.php'); ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">PWD's Profile</h1>
          </div>

          <!-- Content Row -->

          <!-- Content Row -->
           <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-12">
               <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary ">LIST OF PWD</h6>
                  <div class="dropdown no-arrow">
                    <select name="gender_filter" id="gender_filter" class="text-boot">
                      <option value="">Select Gender</option>
                      <option>MALE</option>
                      <option>FEMALE</option>
                    </select>


                    <select name="disab_filter" id="disab_filter" class="text-boot">
                      <option value="">Select Disability</option>
                      <option>Psychosocial Disability</option>
                      <option>Mental Disability</option>
                      <option>Hearing Disability</option>
                      <option>Visial Disability</option>
                      <option>Speech impairment</option>
                      <option>Learning Disability</option>
                      <option>Orthopedic (Musculoskeletal) Disability</option>
                    </select>

                    <select name="address_filter" id="address_filter" class="text-boot">
                      <option value="">Select Address</option>
                       <?php echo select_address(); ?>
                    </select>

                    <input type="text" name="filter_name" id="filter_name" class="text-boot" placeholder="Search Name or ID">

                    <button class="btn btn-sm btn-dark" onclick="show_pwd();"><i class="fa fa-search"></i></button>
                    <button class="btn btn-sm btn-primary" onclick="print_page();"><i class="fa fa-print"></i></button>

                    <a class="btn btn-sm btn-success" style="display: none;" href="#" role="button" aria-haspopup="true" aria-expanded="false" data-toggle="modal" data-backdrop="static" data-target="#registerModal">
                      <i class="fas fa-plus fa-sm fa-fw text-white-400"></i>
                    </a>
                   <!--  <a class="dropdown-toggle btn btn-sm btn-dark" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-white-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Option</div>
                      <a class="dropdown-item" href="#"><i class="fa fa-print"></i> Print</a>
                    </div> -->
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <!-- <button class="btn btn-sm btn-danger" onclick="get_value()">Try</button> -->
                  <div class="table-">
                <table class="table table-bordered " id="tbl_pwd"  cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Gender</th>
                      <th>Address</th>
                      <th>Contact Number</th>
                      <th>Disability</th>
                      <th>Date Registered</th>
                      <th>Option</th>
                    </tr>
                  </thead>
                  <tbody id="pwd_data">
                  </tbody>
                </table>
              </div>
                </div>
              </div>

            </div>
          </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
      <script type="text/javascript">
      
      </script>
      <!-- Footer -->

      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>

   <div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Registration</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button><input type="hidden" name="profile_id" id="profile_id">
        </div>
        <div class="modal-body">
         <div class="row">
                  <div class="col-sm-6 form-group">
                    <label for="fn">Firstname</label>
                    <input type="text" name="fn" id="fn" class="form-control" placeholder="Enter Firstname...">
                    <span class="text-danger" id="fn_alert"></span>
                  </div>
                  
                  <div class="col-sm-6 form-group">
                    <label for="mn">Middlename</label>
                    <input type="text" name="mn" id="mn" class="form-control" placeholder="Enter Middlename...">
                    <span class="text-danger" id="mn_alert"></span>
                  </div>

                  <div class="col-sm-6 form-group">
                    <label for="ln">Lastname</label>
                    <input type="text" name="ln" id="ln" class="form-control" placeholder="Enter Lastname...">
                    <span class="text-danger" id="ln_alert"></span>
                  </div>
                  <div class="col-sm-6 form-group">
                    <label for="bdate">Birthdate</label>
                    <input type="date" name="bdate" id="bdate" class="form-control" placeholder="Enter Lastname...">
                    <span class="text-danger" id="date_alert"></span>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-6 form-group">
                    <label for="gender">Sex</label>
                    <select class="form-control" id="gender">
                      <option selected="" disabled="" value="">Select your gender</option>
                      <option>MALE</option>
                      <option>FEMALE</option>
                    </select>
                    <span class="text-danger" id="gender_alert"></span>
                  </div>

                  <div class="col-sm-6 form-group">
                    <label for="blood_type">Blood Type</label>
                    <select class="form-control" id="blood_type">
                      <option selected="" disabled="" value="">Select your blood type</option>
                      <option value="AB+">AB-positive</option>
                      <option value="AB-">AB-negative</option>
                      <option value="B+">B-positive</option>
                      <option value="B-">B-negative</option>
                      <option value="A+">A-positive</option>
                      <option value="A-">A-negative</option>
                      <option value="O+">O-positive</option>
                      <option value="O-">O-negative</option>
                    </select>
                    <span class="text-danger" id="blood_type_alert"></span>
                  </div>

                   <div class="col-sm-12 form-group">
                    <label for="disability">Disability</label>
                    <select name="disability" id="disability" class="form-control">
                      <option></option>
                      <option>Psychosocial Disability</option>
                      <option>Mental Disability</option>
                      <option>Hearing Disability</option>
                      <option>Visial Disability</option>
                      <option>Speech impairment</option>
                      <option>Learning Disability</option>
                      <option>Orthopedic (Musculoskeletal) Disability</option>
                    </select>
                    

                    <span class="text-danger" id="dis_alert"></span>
                  </div>

                  <div class="col-sm-12 form-group">
                        <label for="address">Address</label>
                    <select  name="address" id="address" class="form-control">
                      <option></option>
                      <?php echo select_address(); ?>
                    </select>
                    <span class="text-danger" id="address_alert"></span>
                  </div>


                  <div class="col-sm-6 form-group">
                    <label for="email">Email</label>
                    <input type="text" name="email" id="email" class="form-control" placeholder="Enter Email address..." autocomplete="off">
                    <span class="text-danger" id="email_alert"></span>
                  </div>
                   
                   <div class="col-sm-6 form-group">
                    <label for="email">Contact Number</label>
                    <input type="text" name="cp_number" id="cp_number" maxlength="11" onkeypress="return num_only(event);" class="form-control" placeholder="Enter contact number..." autocomplete="off">
                    <span class="text-danger" id="cp_number_alert"></span>
                  </div>
                  
                  <div class="col-sm-12">
                     <div class="divider-custom">
                      <div class="divider-custom-line"></div>
                      <div class="divider-custom-icon">
                       <small>IN CASE OF EMERGENCY</small>
                      </div>
                      <div class="divider-custom-line"></div>
                    </div>
                  </div>
                  <div class="col-sm-6 form-group">
                    <label for="gname">Name</label>
                    <input type="text" name="gname" id="gname" class="form-control" placeholder="Enter guardian fullname...">
                    <span class="text-danger" id="gname_alert"></span>
                  </div>


                  <div class="col-sm-6 form-group">
                    <label for="contacts">Contact number</label>
                    <input type="text" name="contacts" id="contacts" maxlength="11" onkeypress="return num_only(event);" class="form-control" placeholder="Enter guardian contact number...">
                    <span class="text-danger" id="contact_alert"></span>
                  </div>
                </div>

        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <button class="btn btn-primary" id="btn_submit" onclick="register_walkIn_staff(3);"><i class="fa fa-save"></i> Register</button>
        </div>
      </div>
    </div>
  </div>

  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>


<?php 
include('footer.php');  
include('modal.php'); 
?>
</body>

</html>

