<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>
<body id="page-top" onload="show_database_backup();">

  <!-- Page Wrapper -->
  <div id="wrapper">

      <?php include('sidenav.php'); ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">PWD's Profile</h1>
          </div>

          <!-- Content Row -->

          <!-- Content Row -->
           <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-12">
               <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary ">Database Backup</h6>
                  <div class="dropdown no-arrow">
                        <button class=" btn btn-success mr-1" onclick="backup_db();"><i class="fa fa-database"></i> Backup</button>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <!-- <button class="btn btn-sm btn-danger" onclick="get_value()">Try</button> -->
                  <div class="table-responsive">
                    <table class="table table-bordered" id="tbl_database">
                      <thead>
                        <tr>
                          <th>File Name</th>
                          <th width="5%" class="text-center" >Option</th>
                        </tr>
                      </thead>
                      <tbody id="data_records">
                      </tbody>
                    </table>
              </div>
                </div>
              </div>

            </div>
          </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->

      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>

  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>


<?php 
include('footer.php');  
include('modal.php'); 
?>
</body>

</html>


<script type="text/javascript">
  function show_database_backup(){
    $.ajax({
      type:"POST",
      url:url,
      data:"action=database_files",
      cache:false,
      success:function(data){
        $("#data_records").html(data);
        $("#tbl_database").dataTable();

      }
    })
  }



 

  function backup_db(){
    swal({
      title: "Are you sure?",
      text: "Do you want to create backup database ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
       $.ajax({
          type:"POST",
          url:url,
          data:"action=backup_db",
          cache:false,
          success:function(data){
            console.log(data.trim());
            if (data.trim() == 1) {
              show_database_backup();
              swal("Success","Database Backup success!","success");
            }else{
              console.log(data,trim());
            }
          }
        });
    });
   
  }

  function execute_backup(dbname,filepath){
      swal({
        title: "Dangerous!",
        text: "Do you want to execute "+dbname+"? \n\n Current record will be permanently deleted!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
      },
      function(){
         $.ajax({
            type:"POST",
            url:url,
            data:"action=execute_db" + '&file_path=' + filepath,
            cache:false,
            beforeSend:function(){
                // swal({title: "Loading...",
                //     text: "Please Wait! \n Do not turn off your computer!\n\n",
                //     showCancelButton: false,
                //     showConfirmButton: false
                //   });             
            },
            success:function(data){
              console.log(data.trim());
              if (data.trim() == 0) {
                show_database_backup();
                swal("Success","Import successfuly !","success");
              }else{
                swal("Success","You have "+data.trim()+" error!","error");
                console.log(data,trim());
              }
            }
          });
      });
  }
</script>
