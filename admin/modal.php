<!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to leave your account.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="../logout.php">Logout</a>
        </div>
      </div>
    </div>
  </div>


  <!-- Logout Modal-->
  <div class="modal fade" id="requirements_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Requirements</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="text-dark h4" id="name_requirements"></div>
           <div class="table-responsive">
            <table class="table table-bordered" id="requirementTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th class="text-truncate">Filename</th>
                  <th class="text-truncate">Date Uploaded</th>
                  <th class="text-truncate">Action</th>
                </tr>
              </thead>
              <tbody id="view_files">
              </tbody>
            </table>
          </div>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>


   <!-- Logout Modal-->
  <div class="modal fade" id="requestModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">New Request</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
            
              <div class="col-xl-12 col-sm-12">
                <div class="form-group">
                  <label for="due_date">Schedule</label>
                  <input type="date" name="due_date" id="due_date" class="form-control">
                  <span id="alert_duedate" class="text-danger"></span>
                </div>
              </div>

              <div class="col-xl-12 col-sm-12">
                <div class="form-group">
                  <label for="due_date">Request Type</label>
                  <select class="form-control" id="type_request">
                      <option selected="" disabled="" value="">Select Request type</option>
                      <option>New</option>
                      <option>Renew</option>
                  </select>
                  <span id="alert_request" class="text-danger"></span>
                </div>
              </div>
            
            <div class="col-xl-12 col-sm-12">
              <p class="text-dark"><span class="fa-2x text-danger">Note</span>:  Please upload your supporting documents <br> <big>(Valid ID or Birthcertificate and 2x2 picture)</big> and we will review your request. </p>
              <a href="files.php" target="_blank">Upload now!</a>
            </div>
            
            </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button class="btn btn-success" onclick="make_request();"><i class="fa fa-paper-plane"></i> Send Request</button>
        </div>
      </div>
    </div>
  </div>

