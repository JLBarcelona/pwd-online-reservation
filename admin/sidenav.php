  <!-- Sidebar -->
    <ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background-color:#222;">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
        <div class="sidebar-brand-icon ">
          <i class="fas fa-user-circle"></i>
        </div>
        <div class="sidebar-brand-text mx-3"><?php echo ($auth['user_type'] == 1)? 'Administrator' : 'Staff' ?></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">
      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link " href="index.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Profile Management
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link" href="pwd.php"  aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-users"></i>
          <span>PWD's Profiles</span>
        </a>
      </li>

       <li class="nav-item">
        <a class="nav-link" href="staff.php"  aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-users-cog"></i>
          <span>Staff's Profiles</span>
        </a>
      </li>

      <!-- Nav Item - Utilities Collapse Menu -->

      <!-- Divider -->
      <hr class="sidebar-divider">

      <div class="sidebar-heading">
        Request Management
      </div>

      <li class="nav-item">
        <a class="nav-link" href="request.php"  aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-bell "></i>
          <span>Pending Request</span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="approve.php"  aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-check-circle "></i>
          <span>Approve Request</span>
        </a>
      </li>

        <li class="nav-item">
        <a class="nav-link" href="disapprove.php"  aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-times-circle "></i>
          <span>Disapprove Request</span>
        </a>
      </li>

   

       <hr class="sidebar-divider">
       <div class="sidebar-heading">
        Options
      </div>

       <li class="nav-item">
        <a class="nav-link" href="setting.php"  aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cogs "></i>
          <span>Account Settings</span>
        </a>
      </li>

      <?php if ($auth['user_type'] == 1): ?> 
       <li class="nav-item">
        <a class="nav-link" href="db_backup.php"  aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cogs "></i>
          <span>Database Backup</span>
        </a>
      </li>

       <li class="nav-item">
        <a class="nav-link" href="logs.php"  aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-list-alt "></i>
          <span>Activity Logs</span>
        </a>
      </li>
      <?php endif ?>

     <!--  <li class="nav-item">
        <a class="nav-link" href="#"  aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-list "></i>
          <span>Activity Logs</span>
        </a>
      </li> -->

        <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-backdrop="static" data-target="#logoutModal"  aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-sign-out-alt "></i>
          <span>Logout</span>
        </a>
      </li>


         <!-- Nav Item - Pages Collapse Menu -->
      
      <br>

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">


            <!-- Nav Item - Messages -->
            <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-bell fa-fw"></i>
                <!-- Counter - Messages -->
                <span class="badge badge-danger badge-counter" id="counter_alert"></span>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="messagesDropdown">
                <h6 class="dropdown-header">
                  Notification
                </h6>
                <div id="notification_data"></div>
                <!-- alert -->
               
                <!-- alert -->
                <a class="dropdown-item text-center small text-gray-500" href="request.php">Show more pending request</a>
              </div>
            </li>

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo ucfirst($auth['fname']).' '.ucfirst($auth['lname']) ?></span>
                <img class="img-profile rounded-circle" src="../webroot/img/site/2x2.jpg">
              </a>
              <!-- Dropdown - User Information -->
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->