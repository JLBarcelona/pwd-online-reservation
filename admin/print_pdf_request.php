<?php
session_start();
require('../config.php');
require('../vendor/FPDF/html_table.php');

$gender_filter = (isset($_REQUEST['gender_filter']))? $_REQUEST['gender_filter'] : '';
$address_filter = (isset($_REQUEST['address_filter']))? $_REQUEST['address_filter'] : '';
$filter_name = (isset($_REQUEST['filter_name'])) ? $_REQUEST['filter_name'] : '';
$disab_filter = (isset($_REQUEST['disab_filter'])) ? $_REQUEST['disab_filter'] : '';

$status = '';




// $gender_filter = $_POST['gender_filter'];
// $address_filter = $_POST['address_filter'];
// $filter_name = $_POST['filter_name'];
// $disab_filter = $_POST['disab_filter'];

$where_disab = '';

$where_address = '';
$where_gender = '';

$where_filter = '';

if (!empty($gender_filter)) {
	$where_gender = "and b.gender like '%$gender_filter%'";
}

if (!empty($address_filter)) {
	$where_address = "and b.address like '%$address_filter%'";
}

if (!empty($filter_name)) {
	$where_filter = "and (b.generated_id like '%$filter_name%' or b.fname like '%$filter_name%' or b.mname like '%$filter_name%' or b.lname like '%$filter_name%' or concat(b.fname,' ',b.lname) like '%$filter_name%')";
}

if (!empty($disab_filter)) {
	$where_disab = "and b.disability_type like '%$disab_filter%'";
}

$data = array();


$types = $disab_filter = (isset($_REQUEST['type'])) ? $_REQUEST['type'] : '';;


if ($types == 1) {
	$status = 'Approved';
}else if ($types == 3) {
	$status = 'Disapprove';
}else if ($types == 0) {
	$status = 'Pending';
}

$data = array('type' => intval($types));



$sql = "SELECT a.*,b.*,c.* from tbl_request a left join tbl_profile b on a.profile_id=b.profile_id left join tbl_account c on a.profile_id=c.profile_id where a.approved = :type and a.date_deleted is NULL and c.user_type=3 ".$where_gender." ".$where_address."  ".$where_filter." ".$where_disab." ";

$prep = $con->prepare($sql);
$prep->execute($data);


$data_output = '';

$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(1);
$pdf->SetFont('Times','B',15);
$pdf->Cell(0,0,'List of Person\'s With Disabilities',0,1,'C',false);
$pdf->Ln(7);
$pdf->SetFont('Times','B',15);
$pdf->Cell(0,0,$status.' Request.',0,1,'C',false);
$pdf->Ln(5);


$data_output .='<table border="1">
		<thead>
			<tr>
				<td width="200" bgcolor="#D0D0FF">Name</td>
				<td width="160" bgcolor="#D0D0FF">Gender</td>
				<td width="400" bgcolor="#D0D0FF">Address</td>
			</tr>
		</thead>
		<tbody>';
		
		while ($row = $prep->fetch()) {
			$name = ucfirst($row['lname']).', '.ucfirst($row['fname']).' '.$row['mname'];
			$data_output .='<tr>';
				$data_output .= '<td width="200">'.$name.'</td>';
				$data_output .= '<td width="160">'.$row['gender'].'</td>';
				$data_output .= '<td width="400">'. $row['address'].'</td>';
			$data_output .= '</tr>';
		}

		
$data_output .='</tbody>';
$data_output .= '</table>';

// data_output

$pdf->SetFont('Times','',8);
$pdf->WriteHTML($data_output);
$pdf->Ln(15);
$pdf->SetFont('Times','B',10);
$pdf->Cell(0,0,'Prepared by: '. $_SESSION['fullname'],0,1,'R',false);
// $pdf->AddPage();
// $pdf->WriteHTML($crop);
$pdf->Output();
?>
