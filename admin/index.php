<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>
<body id="page-top" onload="show_all_request(4); load_dashboard();">

  <!-- Page Wrapper -->
  <div id="wrapper">

      <?php include('sidenav.php'); ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
          </div>

          <!-- Content Row -->
          <div class="row">
            <!-- Staff -->
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-3 mb-4">
              <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1 ">Total Claimed ID</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800" id="claim_count"></div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-id-card fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-3 mb-4">
              <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">Disapproved Request</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800" id="disapprove_count">0</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-user-times fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-3 mb-4">
              <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Pending Requests</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800" id="pending_count">0</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-bell fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-xl-3 col-md-3 mb-4">
              <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Approved Requests</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800" id="approve_count">0</div>
                    </div>
                    <div class="col-auto">
                      <i class="fas fa-user-check fa-2x text-gray-300"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>


          </div>

            


          <!-- Content Row -->
           <div class="row">

             <div class="col-xl-12 col-md-12 col-sm-12">
               <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">List of available ID's</h6>
                  <div class="dropdown no-arrow">
                 <!--    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div> -->
                    <button class="btn btn-success btn-sm" onclick="send_all_sms();"><i class="fa fa-paper-plane"></i> Send Notification</button>
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="table-responsive">
                  <table class="table table-bordered" id="tbl_pending" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Disability</th>
                      <th>Request Type</th>
                      <th>Date Entry</th>
                      <th>Due Date</th>
                      <th>Option</th>
                    </tr>
                  </thead>
                  <tbody id="pending_data">
                  </tbody>
                </table>
              </div>
                </div>
              </div>

            </div>

            <div class="col-xl-12 col-md-12 col-sm-12">
               <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-success">List of Claimed ID's</h6>
                  <div class="dropdown no-arrow">
                   <!--  <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Dropdown Header:</div>
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <div class="dropdown-divider"></div>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div> -->
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="table-responsive">
                  <table class="table table-bordered" id="tbl_claim" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Address</th>
                      <th>Request Type</th>
                      <th>Claimant Name</th>
                      <th>Date Claimed</th>
                      <!-- <th>Option</th> -->
                    </tr>
                  </thead>
                  <tbody id="claim_data">
                  </tbody>
                </table>
              </div>
                </div>
              </div>

            </div>

          

          
          </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

 
<?php include('footer.php'); include('modal.php'); ?>
</body>

</html>

<script type="text/javascript">
  function send_all_sms(){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=send_all_inbox',
      cache:false,
      beforeSend:function(){
        swal("Loading","Sending Sms please Wait!","info");
      },
      success:function(data){
        console.log(data.trim());
        if (data.trim() == 1) {
          swal("Success","Message sent!","success");
        }else{
          console.log(data.trim());
        }
      }
    });
  }
</script>