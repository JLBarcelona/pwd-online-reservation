<?php
session_start();
require('../config.php');
require('../vendor/FPDF/html_table.php');

$gender_filter = (isset($_REQUEST['gender_filter']))? $_REQUEST['gender_filter'] : '';
$address_filter = (isset($_REQUEST['address_filter']))? $_REQUEST['address_filter'] : '';
$filter_name = (isset($_REQUEST['filter_name'])) ? $_REQUEST['filter_name'] : '';
$disab_filter = (isset($_REQUEST['disab_filter'])) ? $_REQUEST['disab_filter'] : '';



$where_filter = '';
$where_address = '';
$where_gender = '';
$where_disab = '';

if (!empty($gender_filter)) {
$where_gender = "and a.gender like '%$gender_filter%'";
}

if (!empty($address_filter)) {
$where_address = "and a.address like '%$address_filter%'";
}


if (!empty($disab_filter)) {
$where_disab = "and a.disability_type like '%$disab_filter%'";
}

if (!empty($filter_name)) {
	$where_filter = "and (a.generated_id like '%$filter_name%' or a.fname like '%$filter_name%' or a.mname like '%$filter_name%' or a.lname like '%$filter_name%' or concat(a.fname,' ',a.lname) like '%$filter_name%')";
}

$data = array();
$sql ="SELECT a.*,b.* from tbl_profile a left join tbl_account b on a.profile_id=b.profile_id where b.is_approve = 1 and b.user_type= 3 and b.date_deleted is null ".$where_gender." ".$where_address."  ".$where_filter." ".$where_filter." ";

$prep = $con->prepare($sql);
$prep->execute($data);


$data_output = '';

$pdf=new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Ln(1);
$pdf->SetFont('Times','B',15);
$pdf->Cell(0,0,'List of Person\'s With Disabilities',0,1,'C',false);
$pdf->Ln(5);


$data_output .='<table border="1">
		<thead>
			<tr>
				<td width="200" bgcolor="#D0D0FF">Name</td>
				<td width="160" bgcolor="#D0D0FF">Gender</td>
				<td width="400" bgcolor="#D0D0FF">Address</td>
			</tr>
		</thead>
		<tbody>';
		
		while ($row = $prep->fetch()) {
			$name = ucfirst($row['lname']).', '.ucfirst($row['fname']).' '.$row['mname'];
			$data_output .='<tr>';
				$data_output .= '<td width="200">'.$name.'</td>';
				$data_output .= '<td width="160">'.$row['gender'].'</td>';
				$data_output .= '<td width="400">'. $row['address'].'</td>';
			$data_output .= '</tr>';
		}

		
$data_output .='</tbody>';
$data_output .= '</table>';

// data_output

$pdf->SetFont('Times','',8);
$pdf->WriteHTML($data_output);
$pdf->Ln(15);
$pdf->SetFont('Times','B',10);
$pdf->Cell(0,0,'Prepared by: '. $_SESSION['fullname'],0,1,'R',false);
// $pdf->AddPage();
// $pdf->WriteHTML($crop);
$pdf->Output();
?>
