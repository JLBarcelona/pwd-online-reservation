<!DOCTYPE html>
<html lang="en">
<?php include('header.php'); ?>
<body id="page-top" onload="show_all_logs()">

  <!-- Page Wrapper -->
  <div id="wrapper">

      <?php include('sidenav.php'); ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Activity Logs</h1>
          </div>

          <!-- Content Row -->

          <!-- Content Row -->
           <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-12">
               <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-info ">LIST OF ACTIVITY LOGS</h6>
                  <div class="dropdown no-arrow">
                    <!-- <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                      <div class="dropdown-header">Option</div>
                      <a class="dropdown-item" href="#"><i class="fa fa-print"></i> Print</a>
                    </div> -->
                  </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                  <div class="table-responsive">
                <table class="table table-bordered" id="tbl_pending" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Activity</th>
                      <th>Date</th>
                      <!-- <th>Option</th> -->
                    </tr>
                  </thead>
                  <tbody id="pending_data">
                  </tbody>
                </table>
              </div>
                </div>
              </div>

            </div>
          </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->

      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

<?php 
include('footer.php');  
include('modal.php'); 
?>
</body>

</html>
<script type="text/javascript">
  function show_all_logs(){
    $.ajax({
      type:"POST",
      url:url,
      data:'action=show_all_logs',
      cache:false,
      success:function(data){
        $("#pending_data").html(data);
        $("#tbl_pending").DataTable();
        
      }
    });
  }
</script>