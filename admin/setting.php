<!DOCTYPE html>
<html lang="en">
<?php
 include('header.php'); 
?>

<body id="page-top" onload="set_session();">

  <!-- Page Wrapper -->
  <div id="wrapper">

      <?php include('sidenav.php'); ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Content Row -->
           <div class="row">
            <div class="col-xl-12 col-md-12 col-sm-12">
               <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Account Settings</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                 
                 <div class="row">
                  <div class="col-sm-8 " id="accordion">
                  <div class="card mb-2">
                    <div class="card-header bg-primary text-white" data-toggle="collapse" data-parent="#accordion" href="#collapse1">Profile</div>
                    <div class="card-body collapse in" id="collapse1">
                        <!-- Profile -->
                        <div class="row">
                        
                            <div class="col-sm-6 form-group">
                              <label for="fn">Firstname</label>
                              <input type="text" name="fn" id="fn" class="form-control" placeholder="Enter Firstname...">
                              <span class="text-danger" id="fn_alert"></span>
                            </div>
                            
                            <div class="col-sm-6 form-group">
                              <label for="mn">Middlename</label>
                              <input type="text" name="mn" id="mn" class="form-control" placeholder="Enter Middlename...">
                              <span class="text-danger" id="mn_alert"></span>
                            </div>

                            <div class="col-sm-12 form-group">
                              <label for="ln">Lastname</label>
                              <input type="text" name="ln" id="ln" class="form-control" placeholder="Enter Lastname...">
                              <span class="text-danger" id="ln_alert"></span>
                            </div>
                           
                        </div>

                      <div class="row">
                        <div class="col-sm-6 form-group">
                          <label for="gender">Gender</label>
                          <select class="form-control" id="gender">
                            <option selected="" disabled="" value="">Select your gender</option>
                            <option>MALE</option>
                            <option>FEMALE</option>
                          </select>
                          <span class="text-danger" id="gender_alert"></span>
                        </div>

                        <div class="col-sm-6 form-group" style="display: none;">
                            <label for="blood_type">Blood Type</label>
                            <select class="form-control" id="blood_type">
                              <option selected="" disabled="" value="">Select your blood type</option>
                              <option value="AB(pos)">AB-positive</option>
                              <option value="AB(neg)">AB-negative</option>
                              <option value="B(pos)">B-positive</option>
                              <option value="B(neg)">B-negative</option>
                              <option value="A(pos)">A-positive</option>
                              <option value="A(neg)">A-negative</option>
                              <option value="O(pos)">O-positive</option>
                              <option value="O(neg)">O-negative</option>
                            </select>
                            <span class="text-danger" id="blood_type_alert"></span>
                          </div>

                         <div class="col-sm-6 form-group">
                              <label for="bdate">Birthdate</label>
                              <input type="date" name="bdate" id="bdate" class="form-control" placeholder="Enter Lastname...">
                              <span class="text-danger" id="date_alert"></span>
                            </div>

                         <div class="col-sm-6 form-group" style="display: none;">
                          <label for="disability">Disability</label>
                          <input type="text" name="disability" id="disability" class="form-control" placeholder="Enter Disablity...">
                          <span class="text-danger" id="dis_alert"></span>
                        </div>


                        <div class="col-sm-12 form-group">
                          <label for="address">Address</label>
                          <textarea  name="address" id="address" class="form-control" placeholder="Enter Address..."></textarea>
                          <span class="text-danger" id="address_alert"></span>
                        </div>

                         <div class="col-sm-12 text-center" style="display: none;">
                             <small>IN CASE OF EMERGENCY</small>
                             <hr>
                          </div>
                        </div>

                        <div class="row" >
                            <div class="col-sm-6 form-group" style="display: none;">
                            <label for="gname">Name</label>
                            <input type="text" name="gname" id="gname" class="form-control" placeholder="Enter fullname...">
                            <span class="text-danger" id="gname_alert"></span>
                          </div>


                          <div class="col-sm-6 form-group" style="display: none;">
                            <label for="contacts">Contact number</label>
                            <input type="text" name="contacts" id="contacts" maxlength="11" onkeypress="return num_only(event);" class="form-control" placeholder="Enter contact number...">
                            <span class="text-danger" id="contact_alert"></span>
                          </div>

                          <div class="col-sm-12 form-group text-right">
                             <button class="btn btn-success" id="btn_submit" onclick="save_profile();"><i class="fa fa-save"></i> Save</button>
                          </div>
                        </div>
                    </div>
                  </div>

                  
                   <div class="card mb-2">
                    <div class="card-header bg-primary text-white" data-toggle="collapse" data-parent="#accordion" href="#collapse2">Email Address</div>
                    <div class="card-body collapse in" id="collapse2">
                       <div class="col-sm-12 form-group">
                        <label for="email">Email</label>
                        <input type="text" name="email" id="email" class="form-control" placeholder="Enter Email address..." autocomplete="off">
                        <span class="text-danger" id="email_alert"></span>
                      </div>

                       <div class="form-group col-sm-12 text-right">
                        <br>
                        <button class="btn btn-success" id="btn_submit" onclick="save_account_email();"><i class="fa fa-save"></i> Save Email</button>
                      </div>
                    </div>
                  </div>

                    <div class="card mb-2">
                      <div class="card-header bg-primary text-white" data-toggle="collapse" data-parent="#accordion" href="#collapse3">Password</div>
                      <div class="card-body collapse in" id="collapse3">
                        <div class="col-sm-12 form-group">
                          <label for="o_password">Old Password</label>
                          <input type="password" name="o_password" id="o_password" class="form-control" placeholder="Enter old password..." autocomplete="off">
                          <span class="text-danger" id="o_password_alert"></span>
                        </div>

                        <div class="col-sm-12 form-group">
                          <label for="password">New Password</label>
                          <input type="password" name="password" id="password" class="form-control" placeholder="Enter new password..." autocomplete="off">
                          <span class="text-danger" id="password_alert"></span>
                        </div>

                        <div class="col-sm-12 form-group">
                          <label for="cpassword">Confirm Password</label>
                          <input type="password" name="cpassword" id="cpassword" class="form-control" placeholder="Confirm password..." autocomplete="off">
                          <span class="text-danger" id="cpassword_alert"></span>
                        </div>

                        <div class="form-group col-sm-12 text-right">
                          <br>
                          <button class="btn btn-success" id="btn_submit" onclick="save_new_password();"><i class="fa fa-save"></i> Save Password</button>
                        </div>
                      </div>
                    </div>
                   </div>
                  </div>

                  <div class="col-sm-4"></div>
                 </div>


                </div>
              </div>
            </div>
          </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  
<?php 
  include('modal.php');
  include('footer.php'); 
?>


</body>

</html>
