SET FOREIGN_KEY_CHECKS=0;DROP TABLE IF EXISTS `tbl_account`;CREATE TABLE `tbl_account` (
  `account_id` int(255) NOT NULL AUTO_INCREMENT,
  `profile_id` int(255) DEFAULT NULL,
  `email_address` varchar(255) DEFAULT NULL,
  `password` longtext CHARACTER SET utf8,
  `access_token` longtext CHARACTER SET utf8,
  `user_type` int(1) DEFAULT '3',
  `date_registered` date DEFAULT NULL,
  `date_deleted` date DEFAULT NULL,
  `is_approve` int(1) DEFAULT '0',
  `is_forgot` int(1) DEFAULT '0',
  `verification_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`account_id`),
  KEY `profile` (`profile_id`),
  CONSTRAINT `profile` FOREIGN KEY (`profile_id`) REFERENCES `tbl_profile` (`profile_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;INSERT INTO tbl_account (account_id, profile_id, email_address, password, access_token, user_type, date_registered, date_deleted, is_approve, is_forgot, verification_code) VALUES ('12','13','admin@gmail.com','$2a$10$rW94Tzt7IynJAXeWAcmTV.EFM.JBIVaBoJzI2i6wlUm/4BmIt.kO2',null,'1','2019-09-05',null,'1','0','');INSERT INTO tbl_account (account_id, profile_id, email_address, password, access_token, user_type, date_registered, date_deleted, is_approve, is_forgot, verification_code) VALUES ('13','14','sub1@gmail.com','$2a$10$rW94Tzt7IynJAXeWAcmTV.EFM.JBIVaBoJzI2i6wlUm/4BmIt.kO2','$2y$10$ryQF6bUe4hjasLKgykDJ8.Y2xEO4g7y2zv92bWCSwwZinMYy95M46','2','2019-09-05',null,'1','0','');INSERT INTO tbl_account (account_id, profile_id, email_address, password, access_token, user_type, date_registered, date_deleted, is_approve, is_forgot, verification_code) VALUES ('14','16','sub2@gmail.com','$2a$10$rW94Tzt7IynJAXeWAcmTV.EFM.JBIVaBoJzI2i6wlUm/4BmIt.kO2','$2y$10$hHK7rgp4HizjxQCs7DZ/b.mCE7uNBIv/P1yGKn.Vj5RzxisqHF9qq','3','2019-09-05',null,'1','0','');DROP TABLE IF EXISTS `tbl_authsig`;CREATE TABLE `tbl_authsig` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `sign` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;DROP TABLE IF EXISTS `tbl_log`;CREATE TABLE `tbl_log` (
  `log_id` int(255) NOT NULL AUTO_INCREMENT,
  `profile_id` int(255) DEFAULT NULL,
  `message` longtext,
  `date_register` date DEFAULT NULL,
  `date_deleted` date DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `profile_id` (`profile_id`),
  CONSTRAINT `profile_id` FOREIGN KEY (`profile_id`) REFERENCES `tbl_profile` (`profile_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;DROP TABLE IF EXISTS `tbl_profile`;CREATE TABLE `tbl_profile` (
  `profile_id` int(255) NOT NULL AUTO_INCREMENT,
  `generated_id` varchar(255) DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `mname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `blood_type` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `disability_type` varchar(255) DEFAULT NULL,
  `guardian_name` varchar(255) DEFAULT NULL,
  `guardian_number` varchar(255) DEFAULT NULL,
  `profile_type` int(1) DEFAULT '1',
  `date_registered` date DEFAULT NULL,
  `date_deleted` date DEFAULT NULL,
  PRIMARY KEY (`profile_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;INSERT INTO tbl_profile (profile_id, generated_id, fname, mname, lname, birthdate, gender, contact_number, blood_type, address, disability_type, guardian_name, guardian_number, profile_type, date_registered, date_deleted) VALUES ('13',null,'Juan','L.','Dela Cruz','2019-09-01','MALE','09555216863','A+','San Juan Santiago City','None',null,null,'1','2019-09-05','0000-00-00');INSERT INTO tbl_profile (profile_id, generated_id, fname, mname, lname, birthdate, gender, contact_number, blood_type, address, disability_type, guardian_name, guardian_number, profile_type, date_registered, date_deleted) VALUES ('14',null,'Jose','P.','Rizal','2019-01-01','MALE',null,'A+


','Centro West',null,null,null,'1','2019-09-05','0000-00-00');INSERT INTO tbl_profile (profile_id, generated_id, fname, mname, lname, birthdate, gender, contact_number, blood_type, address, disability_type, guardian_name, guardian_number, profile_type, date_registered, date_deleted) VALUES ('15',null,'Apol','Y.','Mabini','2011-01-01','MALE',null,'A+','Calao West',null,null,null,'1','2019-09-05','0000-00-00');INSERT INTO tbl_profile (profile_id, generated_id, fname, mname, lname, birthdate, gender, contact_number, blood_type, address, disability_type, guardian_name, guardian_number, profile_type, date_registered, date_deleted) VALUES ('16','16-8026-441-1473','Jeffrey Luis','A.','Guya','2019-01-01','MALE','09555216863','AB ','Centro West','Orthopedic (Musculoskeletal) disability','Bill Gates','12345555555','1','2019-09-05','0000-00-00');INSERT INTO tbl_profile (profile_id, generated_id, fname, mname, lname, birthdate, gender, contact_number, blood_type, address, disability_type, guardian_name, guardian_number, profile_type, date_registered, date_deleted) VALUES ('18','17-2208-120-1845','JL','S.','Barcelona','2019-01-01','MALE','09555216863','MALE','Abra','None','Bill Gates','12345555555','1','2019-09-09','0000-00-00');DROP TABLE IF EXISTS `tbl_request`;CREATE TABLE `tbl_request` (
  `request_id` int(255) NOT NULL AUTO_INCREMENT,
  `profile_id` int(255) DEFAULT NULL,
  `date_request` date DEFAULT NULL,
  `request_type` varchar(255) DEFAULT NULL,
  `approved` int(1) DEFAULT '0',
  `date_registered` date DEFAULT NULL,
  `date_deleted` date DEFAULT NULL,
  PRIMARY KEY (`request_id`),
  KEY `request_account` (`profile_id`),
  CONSTRAINT `request_account` FOREIGN KEY (`profile_id`) REFERENCES `tbl_profile` (`profile_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;INSERT INTO tbl_request (request_id, profile_id, date_request, request_type, approved, date_registered, date_deleted) VALUES ('7','16','2019-09-27','New','4','2019-09-19','0000-00-00');DROP TABLE IF EXISTS `tbl_requirements`;CREATE TABLE `tbl_requirements` (
  `file_id` int(255) NOT NULL AUTO_INCREMENT,
  `profile_id` int(255) DEFAULT NULL,
  `file_name` longtext,
  `file_path` longtext,
  `file_type` varchar(255) DEFAULT NULL,
  `file_size` double(255,0) DEFAULT NULL,
  `is_photo` int(1) DEFAULT '0',
  `date_registered` date DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `requirements` (`profile_id`),
  CONSTRAINT `requirements` FOREIGN KEY (`profile_id`) REFERENCES `tbl_profile` (`profile_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;INSERT INTO tbl_requirements (file_id, profile_id, file_name, file_path, file_type, file_size, is_photo, date_registered) VALUES ('18','18','user.PNG','5d75e5bd5a29e9.68648249.png','image/png','10141','1','2019-09-09');INSERT INTO tbl_requirements (file_id, profile_id, file_name, file_path, file_type, file_size, is_photo, date_registered) VALUES ('19','18','62.pdf','5d75e64ae6f0b4.07086578.pdf','application/pdf','134079','0','2019-09-09');INSERT INTO tbl_requirements (file_id, profile_id, file_name, file_path, file_type, file_size, is_photo, date_registered) VALUES ('29','16','17795901_1478253198874396_2380371579542396204_n.jpg','5d83211407d6d1.91993422.jpg','image/jpeg','56123','1','2019-09-19');INSERT INTO tbl_requirements (file_id, profile_id, file_name, file_path, file_type, file_size, is_photo, date_registered) VALUES ('30','16','399px-Naomi_Scott.jpg','5d832123620e88.71509366.jpg','image/jpeg','57537','0','2019-09-19');INSERT INTO tbl_requirements (file_id, profile_id, file_name, file_path, file_type, file_size, is_photo, date_registered) VALUES ('31','16','711px-PHP-logo.svg.png','5d8321237c0178.11829338.png','image/png','41313','0','2019-09-19');