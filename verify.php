<?php 
include('config.php');
include('function.php');

$token = $_REQUEST['API_token'];
$id = $_REQUEST['id'];

$data_verify = array('id' => $id);

$verify = "SELECT * from tbl_account Where profile_id = :id and is_approve = 0";

$prep = $con->prepare($verify);
$prep->execute($data_verify);

 if ($prep->rowCount() > 0) {
		$row = $prep->fetch();
		if ($token === $row['access_token']) {
			// Token Accept
			$data = array('id' => $id, 'approve' => 1);
			$sql = "UPDATE tbl_account set is_approve = :approve where profile_id = :id";
			$result = save($con,$data,$sql);
			$success = password_hash('success', PASSWORD_DEFAULT);
			header('location:index.php?verify=$success');
		}else{
			$invalid = password_hash('success', PASSWORD_DEFAULT);
			header('location:index.php?verify=$invalid');
		}
	}else{
			$expired = password_hash('success', PASSWORD_DEFAULT);
			header('location:index.php?verify=$expired');
	}


 ?>