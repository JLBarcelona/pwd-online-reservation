<?php 
include("../vendor/phpqrcode/qrlib.php");
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require '../vendor/vendor/autoload.php';




function save($con,$data,$sql){
	// sample
	// INSERT INTO tbl_employee (firstname,lastname) VALUES(:fn,:ln)
	$prep = $con->prepare($sql);
	$prep->execute($data);

	if ($prep) {
		return 1;
	}
}


function avatar($avatar,$gender,$user_type){
	if (!empty($avatar)) {
		return $avatar;
	}else{
		if ($user_type == 3) {
			return 'img/'.$gender.'.png';
		}else if($user_type == 2){
			return 'img/'.$gender.'P.png';
		}else{
			return 'img/admin.png';
		}
	}
}


// function logs($con,$message,$account_id,$date_entry){
// 	$data = array('message' => $message,
// 				  'account_id' => $account_id,
// 				  'date_entry' => $date_entry
// 				 );
// 	$sql = "INSERT INTO tbl_log(profile_id,message,date_register) VALUES(:account_id,:message,:date_entry)";

// 	if (save($con,$data,$sql) > 0) {
// 		return 1;
// 	}
// }


function get_max_id($con){
	$data = array();
	$sql ="SELECT max(user_id) from tbl_user";

	return max_record($con,$data,$sql,'user_id') + 1;
}

// Get count record
function count_record($con,$data,$sql,$count){
    $result = fetch_record($con,$data,$sql);
    $row = $result->fetch();

    if ($row['count('.$count.')'] > 0) {
    	return $row['count('.$count.')'];
    }else{
    	return 0;
    }
}


// get  max record
function max_record($con,$data,$sql,$count){
    $result = fetch_record($con,$data,$sql);
    $row = $result->fetch();

    if ($row['max('.$count.')'] > 0) {
    	return $row['max('.$count.')'];
    }else{
    	return 0;
    }
}


function get_session($con,$id){
	$data = array('id' => $id);
    $sql = "SELECT * from tbl_user where user_id = :id";
    $result = fetch_record($con,$data,$sql);
    $row = $result->fetch();

    return $row;
}

function fetch_record($con,$data,$sql){
	// sample
	// INSERT INTO tbl_employee (firstname,lastname) VALUES(:fn,:ln)
	$prep = $con->prepare($sql);
	$prep->execute($data);

	return $prep;
}

function iteration($data){
	if (!empty($data)) {
		return $data;
	}else{
		return '';
	}
}


function check_upload_2x2($con,$id){
	$data = array('id' => $id);

	$sql = "SELECT user_id from tbl_requirements where user_id = :id and is_photo = 1";
	
	$prep = $con->prepare($sql);
	$prep->execute($data);

	$row = $prep->fetch();

	return $prep->rowCount();
}


function verify_record($con,$data,$sql){
	$prep = $con->prepare($sql);
	$prep->execute($data);

	$row = $prep->fetch();

	return $prep->rowCount();
}



function userAuth($row){
	$_SESSION['user_id'] = iteration($row['user_id']);
	$_SESSION['fullname'] = iteration($row['fn'].' '.$row['ln']);
	$_SESSION['firstname'] = iteration($row['fn']);
	$_SESSION['middlename'] = iteration($row['mn']);
	$_SESSION['lastname'] = iteration($row['ln']);

	$_SESSION['gender'] = iteration($row['gender']);
	// user types 1 = admin, 2 = staff and 3 = pwd_user
	$_SESSION['user_type'] = iteration($row['user_type']);
	// verifications puposes
	$_SESSION['password'] = iteration($row['password']);
}


function GetLastId($con,$data,$sql){
	// sample
	// INSERT INTO tbl_employee (firstname,lastname) VALUES(:fn,:ln)
	$prep = $con->prepare($sql);
	$prep->execute($data);

	return $con->lastInsertId();
}

function format_date($format,$date){
	return date ($format,strtotime($date));
}

function delete_query($con,$data,$sql){
	// sample
	// DELETE from tbl_employee WHERE id=:id
	$prep = $con->prepare($sql);
	$prep->execute($data);

	if ($prep) {
		return 1;
	}
}

?>



